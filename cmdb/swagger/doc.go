package swagger

import "github.com/go-openapi/spec"

// 文档的相关选项
func Docs(swo *spec.Swagger) {
	swo.Info = &spec.Info{
		InfoProps: spec.InfoProps{
			//标题
			Title: "注册中心",
			//注释信息
			Description: "这里是注释信息",
			//下面是联系人
			Contact: &spec.ContactInfo{
				ContactInfoProps: spec.ContactInfoProps{
					Name:  "联系人A",
					Email: "联系人邮件",
					URL:   "联系人URL",
				},
			},
			//开源协议
			License: &spec.License{
				LicenseProps: spec.LicenseProps{
					Name: "MIT",
					URL:  "http://localhost:8080",
				},
			},
			//版本号
			Version: "版本号",
		},
	}
	//这里是详细配置标签 需要结合path中的tag
	swo.Tags = []spec.Tag{
		{
			TagProps: spec.TagProps{
				Name:        "用户",
				Description: "用户的副标题",
				ExternalDocs: &spec.ExternalDocumentation{
					Description: "点击查找更多",
					URL:         "http://www.baidu.com",
				},
			},
		},
		{
			TagProps: spec.TagProps{
				Name:        "token",
				Description: "token的副标题",
				ExternalDocs: &spec.ExternalDocumentation{
					Description: "点击查找更多",
					URL:         "http://www.baidu.com",
				},
			},
		},
	}

	//设置版本
	swo.BasePath = "/v2"
	//设置主机名
	swo.Host = "www.laohe.icu"
	//设置可选择的协议
	swo.Schemes = []string{"http", "https"}
}
