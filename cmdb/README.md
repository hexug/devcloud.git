# 使用mysql数据库的模板

包括以下部分
+ toml配置文件读取
+ 环境变量配置读取
+ grpc的使用
+ go-restful的使用
+ swagger配置文件的使用
+ 通用脚手架makefile
+ 工程化
+ 数据校验
+ 测试用例json格式化
+ 基于zapper的日志
+ app的注册中心