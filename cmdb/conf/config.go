package conf

import (
	"fmt"
	"regexp"

	"github.com/BurntSushi/toml"
	"github.com/caarlos0/env/v6"
)

var config *Conf

// 使用配置的函数
func C() *Conf {
	if config == nil {
		config = &Conf{
			Mysql: DefaultMysql(),
			Log:   &Log{},
			Grpc:  DefaultGrpc(),
			Http:  DefaultHttp(),
		}
	}
	return config
}

// LoadConfigFromToml 从文件加载配置
func LoadConfigFromToml(path string) {
	if config == nil {
		config = C()
	}
	//从文件中解析
	_, err := toml.DecodeFile(path, config)
	//看下是否报错，或者报错是否为文件无法找到
	if err != nil {
		re := regexp.MustCompile("The system cannot find the file|path specified")
		if ok := re.MatchString(err.Error()); !ok {
			fmt.Printf("正则匹配出错：%s\n", err.Error())
			panic(err)
		}
		fmt.Printf("警告：%s\n", err.Error())
	}
}

// LoadConfigFromEnv 从环境变量加载配置
func LoadConfigFromEnv() {
	if config == nil {
		config = C()
	}
	if err := env.Parse(config); err != nil {
		panic(err)
	}
}
