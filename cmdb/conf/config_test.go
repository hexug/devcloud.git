package conf_test

import (
	"context"
	"testing"

	"gitee.com/hexug/devcloud/cmdb/common/logger"
	"gitee.com/hexug/devcloud/cmdb/conf"
)

var (
	ctx = context.TODO()
)

func init() {
	conf.LoadConfigFromToml("../etc/config.toml")
}

type User struct {
	ID      uint64 `gorm:"primaryKey;autoIncrement"`
	Name    string `gorm:"column:name"`
	Email   string `gorm:"column:email"`
	Age     int    `gorm:"column:age"`
	Enabled bool   `gorm:"column:enabled"`
}

/*
测试使用的建表sql语句

	CREATE TABLE `users` (
	  `id` int unsigned NOT NULL AUTO_INCREMENT,
	  `name` longtext,
	  `email` longtext,
	  `age` tinyint unsigned DEFAULT NULL,
	  PRIMARY KEY (`id`)
	) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
*/
func TestLoadConfig(t *testing.T) {
	db := conf.C().Mysql.ORM().WithContext(ctx).Debug()
	data := &User{
		ID:      19,
		Name:    "abc",
		Email:   "abcs@123.com",
		Age:     23,
		Enabled: false,
	}
	err := db.Save(data).Error
	if err != nil {
		t.Fatal(err)
	}
	logger.L().Debugw("成功1")
	logger.L().Infow("成功2")
	logger.L().Errorw("成功3")

}
