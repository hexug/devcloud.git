package logger_test

import (
	"testing"

	"gitee.com/hexug/devcloud/cmdb/common/logger"
	"gitee.com/hexug/devcloud/cmdb/conf"
)

func init() {
	conf.LoadConfigFromToml("../etc/config.toml")
}
func TestLoger(t *testing.T) {
	logger.L().Debugw("调试信息1")
	logger.L().Infow("调试信息2")
	logger.L().Errorw("调试信息3")
}
