package main

import (
	_ "gitee.com/hexug/devcloud/cmdb/apps/all"
	"gitee.com/hexug/devcloud/cmdb/cmd"
)

func main() {
	cmd.Execute()
}
