package tengxun_test

import (
	"context"
	"os"
	"testing"

	"gitee.com/hexug/devcloud/cmdb/provider/tengxun"
)

func TestCvm(t *testing.T) {
	secretID := os.Getenv("SECRET_ID")
	securetKey := os.Getenv("SECURET_Key")
	c := tengxun.NewTencentClient(secretID, securetKey)
	// c.SetPageSize(1)
	if err := c.SyncHost(context.Background()); err != nil {
		t.Fatal(err.Error())
	}
}
