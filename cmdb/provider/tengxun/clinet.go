package tengxun

import (
	"gitee.com/hexug/devcloud/cmdb/provider"
	"github.com/tencentcloud/tencentcloud-sdk-go/tencentcloud/common"
	"github.com/tencentcloud/tencentcloud-sdk-go/tencentcloud/common/profile"

	//令牌桶
	"github.com/infraboard/mcube/flowcontrol/tokenbucket"
	cvm "github.com/tencentcloud/tencentcloud-sdk-go/tencentcloud/cvm/v20170312"
)

type TencentClient struct {
	SecretId  string
	SecretKey string

	//分页
	PageSize int64

	//数据处理回调函数
	hook provider.ResourceHandleHook

	//令牌桶 控制拉取速度
	tb *tokenbucket.Bucket
}

func NewTencentClient(secretID, securetKey string) *TencentClient {
	return &TencentClient{
		SecretId:  secretID,
		SecretKey: securetKey,

		//这里只需要pagesize 需要全部都拉取出来，分页拉取
		PageSize: 10,
		hook:     provider.DefaultResourceHandleHook,

		// NewBucketWithRate返回一个令牌桶，该桶以每秒速率填充令牌的速率填充到给定的最大容量。
		//由于时钟分辨率有限，在高速率下，实际速率可能与指定速率相差高达1%。
		//以每秒1个的速度获取 桶的容量为5
		tb: tokenbucket.NewBucketWithRate(1, 5),
	}
}

func (t *TencentClient) SetResourceHandleHook(hook provider.ResourceHandleHook) {
	t.hook = hook
}

func (t *TencentClient) SetPageSize(page_size int64) {
	t.PageSize = page_size
}

// 新建凭证对象
func (t *TencentClient) NewCredential() *common.Credential {
	return common.NewCredential(
		t.SecretId,
		t.SecretKey,
	)
}

// 实例化一个请求客户端
func (t *TencentClient) NewCvmClient() (client *cvm.Client, err error) {
	// 实例化一个client选项，可选的，没有特殊需求可以跳过
	cpf := profile.NewClientProfile()
	//设置资源请求节点 不同类型的资源请求节点不同
	cpf.HttpProfile.Endpoint = "cvm.tencentcloudapi.com"
	// 实例化要请求产品的client对象,clientProfile是可选的
	//设置请求节点相关的参数，还有对应的凭证对象
	return cvm.NewClient(t.NewCredential(), "ap-nanjing", cpf)
}
