package provider

import (
	"context"
	"fmt"

	"gitee.com/hexug/devcloud/cmdb/apps/resource"
	"gitee.com/hexug/devcloud/cmdb/test/format"
)

// 签名
type ResourceHandleHook func(context.Context, *resource.Resource)

// 实现的一个基础默认回调函数
func DefaultResourceHandleHook(ctx context.Context, in *resource.Resource) {
	fmt.Println(format.FormatToJson(in))
}
