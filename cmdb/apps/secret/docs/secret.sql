DROP TABLE IF EXISTS `secrets`;
CREATE TABLE `secrets` (
  `id` int NOT NULL,
  `create_at` bigint DEFAULT NULL,
  `description` text COLLATE utf8mb4_general_ci,
  `provider` tinyint DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `api_id` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `api_secret` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `request_rate` int DEFAULT NULL,
  `enabled` tinyint DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `api_id` (`api_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;