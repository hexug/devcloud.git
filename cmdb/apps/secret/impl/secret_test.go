package impl_test

import (
	"context"
	"os"
	"testing"

	"gitee.com/hexug/devcloud/cmdb/apps"
	_ "gitee.com/hexug/devcloud/cmdb/apps/all"
	"gitee.com/hexug/devcloud/cmdb/apps/resource"
	"gitee.com/hexug/devcloud/cmdb/apps/secret"
	"gitee.com/hexug/devcloud/cmdb/conf"
	"gitee.com/hexug/devcloud/cmdb/test/format"
)

var (
	svc secret.Server
	ctx = context.TODO()
)

func init() {
	conf.LoadConfigFromToml("../../../etc/config.toml")
	apps.GetApp(secret.AppName).Init()
	apps.GetApp(resource.AppName).Init()
	svc = apps.GetApp(secret.AppName).(secret.Server)
}

func TestCreateSecret(t *testing.T) {
	csr := secret.NewCreateSecretRequest()
	csr.ApiId = os.Getenv("SECRET_ID")
	csr.ApiSecret = os.Getenv("SECURET_Key")
	csr.Description = "腾讯云"
	csr.Provider = secret.PROVIDER_TENCENT_CLOUD
	s, err := svc.CreateSecret(ctx, csr)
	if err != nil {
		t.Fatal(err.Error())
	}
	t.Log(format.FormatToJson(s))
}
func TestQuerySecret(t *testing.T) {
	qsr := secret.NewQuerySecretRequest()
	qsr.PageSize = 2
	qsr.PageNumber = 2
	s, err := svc.QuerySecret(ctx, qsr)
	if err != nil {
		t.Fatal(err.Error())
	}
	t.Log(format.FormatToJson(s))
}
func TestDescribeSecret(t *testing.T) {
	dsr := secret.NewDescribeSecretRequest()
	dsr.Id = "9d581a98-03fe-4833-a051-9c54d21c4407"
	s, err := svc.DescribeSecret(ctx, dsr)
	if err != nil {
		t.Fatal(err.Error())
	}
	t.Log(format.FormatToJson(s))
}
func TestSyncResource(t *testing.T) {
	dsr := secret.NewDescribeSecretRequest()
	dsr.Id = "27fb294e-7d87-4fb2-b3cb-64b0c1c9c47a"
	err := svc.SyncResource(ctx, dsr)
	if err != nil {
		t.Fatal(err.Error())
	}
}
