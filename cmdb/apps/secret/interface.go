package secret

import context "context"

var (
	AppName = "secrets"
)

type Server interface {
	RPCServer
	SyncResource(ctx context.Context, in *DescribeSecretRequest) error
}
