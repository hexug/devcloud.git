package apps

import (
	"errors"
	"fmt"

	"google.golang.org/grpc"
)

var (
	GrpcObjLib = map[string]GrpcObj{}
)

type GrpcObj interface {
	AppObj
	ReagisterGrpc(*grpc.Server)
}

func RegisterGrpc(obj GrpcObj) {
	if _, ok := GrpcObjLib[obj.Name()]; ok {
		panic(errors.New("Grpc " + obj.Name() + " 已注册过！"))
	} else {
		GrpcObjLib[obj.Name()] = obj
		fmt.Printf("Grpc %s 注册成功。\n", obj.Name())
	}
}

func GetGrpc(name string) GrpcObj {
	if value, ok := GrpcObjLib[name]; ok {
		return value
	} else {
		panic(errors.New("Grpc" + name + "尚未注册！"))
	}
}
