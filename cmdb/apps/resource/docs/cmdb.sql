SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;



-- ----------------------------
-- Table structure for resource_meta
-- ----------------------------
DROP TABLE IF EXISTS `metas`;
CREATE TABLE `metas` (
  `id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '全局唯一Id, 直接使用个云商自己的Id',
  `domain` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '资源所属域',
  `namespace` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '资源所属空间',
  `env` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '资源所属环境',
  `create_at` int DEFAULT NULL COMMENT '创建时间',
  `delete_at` int DEFAULT NULL COMMENT '删除时间',
  `delete_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '删除人',
  `sync_at` int DEFAULT NULL COMMENT '同步时间',
  `sync_by` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '同步人',
  `credential_id` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '关联的同于同步的凭证 id',
  `serial_number` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '序列号',
  `extra` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'extra',
  `spec_hash` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '规格数据Hash',
  `cost_hash` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '费用数据Hash',
  `status_hash` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '状态数据Hash',
  `tag_hash` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '标签数据Hash',
  `relation_hash` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '关系数据Hash',
  `custom_hash` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '资源自定义属性Hash',
  PRIMARY KEY (`id`),
  KEY `domain` (`domain`),
  KEY `namespace` (`namespace`),
  KEY `env` (`env`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


-- ----------------------------
-- Table structure for resource_spec
-- ----------------------------
DROP TABLE IF EXISTS `specs`;
CREATE TABLE `specs` (
  `id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '资源Id',
  `vendor` tinyint NOT NULL COMMENT '资源厂商',
  `resource_type` tinyint NOT NULL COMMENT '资源类型',
  `region` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '地域',
  `zone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '区域',
  `owner` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '资源归属账号',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '资源名称',
  `category` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '资源种类',
  `type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '规格',
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '描述',
  `expire_at` int NOT NULL COMMENT '过期时间',
  `update_at` int DEFAULT NULL COMMENT '更新时间',
  `cpu` int DEFAULT NULL COMMENT '资源占用Cpu数量',
  `gpu` int DEFAULT NULL COMMENT 'GPU数量',
  `memory` int DEFAULT NULL COMMENT '资源使用的内存, 单位M',
  `storage` int DEFAULT NULL COMMENT '资源使用的存储, 单位G',
  `band_width` int DEFAULT NULL COMMENT '公网IP带宽, 单位M',
  `extra` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '额外的通用属性',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


-- ----------------------------
-- Table structure for resource_status
-- ----------------------------
DROP TABLE IF EXISTS `status`;
CREATE TABLE `status` (
  `id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '资源Id',
  `phase` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '资源当前状态',
  `lock_mode` tinyint NOT NULL COMMENT '实例锁定模式; Unlock：正常；ManualLock：手动触发锁定；LockByExpiration：实例过期自动锁定；LockByRestoration：实例回滚前的自动锁定；LockByDiskQuota：实例空间满自动锁定',
  `lock_reason` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '锁定原因',
  `public_address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '公网IP, 或者域名',
  `private_address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '内网IP, 或者域名',
  `describe` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


-- ----------------------------
-- Table structure for resource_tag
-- ----------------------------
DROP TABLE IF EXISTS `tags`;
CREATE TABLE `tags` (
  `resource_id` varchar(255) COLLATE utf8mb4_general_ci NOT NULL COMMENT '资源Id',
  `t_key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '标签的名称',
  `t_value` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '标签的值',
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '值的描述信息',
  `weight` int DEFAULT NULL COMMENT '标签权重',
  `purpose` tinyint DEFAULT NULL COMMENT '标签用途',
  `read_only` tinyint DEFAULT NULL COMMENT 'read_only',
  `hidden` tinyint DEFAULT NULL COMMENT 'tinyint',
  `delete_at` int DEFAULT NULL COMMENT '删除时间',
  `delete_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `update_at` int NOT NULL,
  `update_by` varbinary(255) DEFAULT NULL,
  `extra` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`resource_id`,`t_key`,`t_value`),
  UNIQUE KEY `idx_id` (`t_key`,`t_value`,`resource_id`) USING BTREE COMMENT '一个资源同一个key value只允许有一对',
  KEY `idx_key` (`t_key`) USING BTREE,
  KEY `idx_value` (`t_value`) USING BTREE,
  KEY `resource_id` (`resource_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='资源标签';


-- ----------------------------
-- Table structure for resource_cost
-- ----------------------------
DROP TABLE IF EXISTS `costs`;
CREATE TABLE `costs` (
  `id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '资源Id',
  `pay_mode` tinyint NOT NULL COMMENT '支付方式',
  `pay_mode_detail` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '字符说明',
  `sale_price` decimal(12,4) DEFAULT NULL COMMENT '官网价,原价（分）',
  `real_cost` decimal(12,4) DEFAULT NULL COMMENT '实际支付金额（分）',
  `policy` float(4,2) DEFAULT NULL COMMENT '折扣率',
  `unit_price` decimal(12,4) DEFAULT NULL COMMENT '单价（分）',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;




-- ----------------------------
-- Table structure for secrets
-- ----------------------------
DROP TABLE IF EXISTS `secrets`;
CREATE TABLE `secrets`  (
  `id` varchar(64) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `create_at` bigint(0) NOT NULL,
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `provider` tinyint(1) NOT NULL,
  `address` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `api_key` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `api_secret` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `request_rate` int(0) NOT NULL,
  `enabled` tinyint(0) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `idx_key`(`api_key`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;