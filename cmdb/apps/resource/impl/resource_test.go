package impl_test

import (
	"context"
	"fmt"
	"testing"

	"gitee.com/hexug/devcloud/cmdb/apps"
	_ "gitee.com/hexug/devcloud/cmdb/apps/all"
	"gitee.com/hexug/devcloud/cmdb/apps/resource"
	"gitee.com/hexug/devcloud/cmdb/conf"
	"gitee.com/hexug/devcloud/cmdb/test/format"
)

var (
	svc resource.Server
	ctx = context.TODO()
)

func init() {
	conf.LoadConfigFromToml("../../../etc/config.toml")
	apps.GetApp(resource.AppName).Init()
	svc = apps.GetApp(resource.AppName).(resource.Server)
}

func TestPut(t *testing.T) {
	r := resource.NewResource()
	r.AddTag(resource.NewKVTag(resource.TAG_PURPOSE_THIRDPARTY, "abc", "111"))
	r.AddTag(resource.NewKVTag(resource.TAG_PURPOSE_THIRDPARTY, "aaaa", "2222"))
	r.AddTag(resource.NewKVTag(resource.TAG_PURPOSE_THIRDPARTY, "ccc", "ddd"))
	m := resource.NewMeta()
	r.Meta = m
	sp := resource.NewSpec()
	r.Spec = sp
	c := resource.NewCost()
	r.Cost = c
	s := resource.NewStatus()
	r.Status = s

	m.Id = "test1"
	m.Domain = "default"
	m.Namespace = "default"

	sp.Vendor = resource.VENDOR_HUAWEI
	sp.ResourceType = resource.TYPE_HOST
	sp.Region = "深圳"
	sp.Name = "主机"
	sp.Cpu = 2
	sp.Memory = 4096

	c.PayMode = resource.PAY_MODE_PRE_PAY
	c.SalePrice = 25.6
	c.RealCost = 12.5

	s.Phase = "规划中"
	s.LockMode = resource.LOCK_MODE_UNLOCK
	s.PublicAddress = []string{"1.1.1.1", "2.2.2.2"}
	res, err := svc.Put(ctx, r)
	if err != nil {
		t.Fatal(err.Error())
	}
	t.Log(format.FormatToJson(res))
}

func TestDelete(t *testing.T) {
	dr := resource.NewDeleteRequest()
	dr.Id = "test1"
	res, err := svc.Delete(ctx, dr)
	if err != nil {
		t.Fatal(err.Error())
	}
	t.Log(format.FormatToJson(res))
}

func TestSearch(t *testing.T) {
	sr := resource.NewSearchRequest()
	// sr.Vendor = resource.VENDOR_ALIYUN.Enum()
	sr.PageSize = 1
	sr.PageNumber = 2
	// sr.Keywords = "2.2.2.2"
	sr.WithTags = true
	reset, err := svc.Search(ctx, sr)
	if err != nil {
		t.Fatal(err)
	}
	fmt.Println(format.FormatToJson(reset))
}
