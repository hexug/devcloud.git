package impl

import (
	"fmt"

	"gitee.com/hexug/devcloud/cmdb/apps"
	"gitee.com/hexug/devcloud/cmdb/apps/resource"
	"gitee.com/hexug/devcloud/cmdb/conf"
	"google.golang.org/grpc"
	"gorm.io/gorm"
)

var _ resource.Server = (*implServer)(nil)
var _ resource.RPCServer = (*implServer)(nil)
var _ apps.AppObj = (*implServer)(nil)
var _ apps.GrpcObj = (*implServer)(nil)

type implServer struct {
	resource.UnimplementedRPCServer
	db *gorm.DB
}

var (
	svc = &implServer{}
)

func (i *implServer) Name() string {
	return resource.AppName
}
func (i *implServer) Init() error {
	i.db = conf.C().ORM().Debug()
	if i.db == nil {
		return fmt.Errorf("数据库 %s 获取失败,疑似尚未注册", resource.AppName)
	}
	return nil
}
func (i *implServer) ReagisterGrpc(s *grpc.Server) {
	resource.RegisterRPCServer(s, i)
}

func init() {
	apps.RegisterApp(svc)
	apps.RegisterGrpc(svc)
}
