package resource

import context "context"

var (
	AppName = "resources"
)

type Server interface {
	RPCServer
	Put(ctx context.Context, in *Resource) (*Resource, error)
	Delete(ctx context.Context, in *DeleteRequest) (*Resource, error)
}
