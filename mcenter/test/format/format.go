package format

import (
	"encoding/json"
)

func FormatToJson(i any) string {
	res, _ := json.MarshalIndent(i, "", "   ")
	return string(res)
}
