package meta

import (
	"time"

	"github.com/google/uuid"
)

func NewMeta() *Meta {
	now := time.Now().Unix()
	return &Meta{
		Id:        uuid.New().String(),
		CreatedAt: now,
		UpdatedAt: now,
	}
}
