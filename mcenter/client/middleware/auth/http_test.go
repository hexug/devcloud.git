package auth_test

import (
	"log"
	"net/http"
	"testing"

	"gitee.com/hexug/devcloud/mcenter/client/middleware/auth"
	"gitee.com/hexug/devcloud/mcenter/client/rpc"
	"github.com/emicklei/go-restful/v3"
)

type UserResource struct {
}

func (u UserResource) Register(container *restful.Container) {
	//创建一个路由组
	ws := new(restful.WebService)
	//设置路由组的根路由
	ws.
		Path("/").
		//设置可以用来解析的格式
		Consumes(restful.MIME_XML, restful.MIME_JSON).
		//设置返回数据的格式
		Produces(restful.MIME_JSON, restful.MIME_XML) // you can specify this per route as well
	//注册子路由到根路由上
	ws.Route(ws.GET("/").To(func(r1 *restful.Request, r2 *restful.Response) {
		r2.Write([]byte("成功"))
	}))
	//将路由组添加到容器中
	container.Add(ws)
}

func TestXxx(t *testing.T) {
	wsContainer := restful.DefaultContainer
	u := UserResource{}
	client, err := rpc.NewClient(rpc.NewConfig())
	if err != nil {
		t.Fatal(err)
	}
	//注册路由
	u.Register(wsContainer)
	wsContainer.Filter(auth.NewHttpAuther(client).AuthFunc)

	log.Printf("start listening on localhost:8080")

	//启服务
	server := &http.Server{Addr: ":8080", Handler: wsContainer}
	t.Fatal(server.ListenAndServe())
}
