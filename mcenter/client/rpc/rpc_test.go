package rpc_test

import (
	"context"
	"testing"

	"gitee.com/hexug/devcloud/mcenter/apps/token"
	"gitee.com/hexug/devcloud/mcenter/apps/user"
	"gitee.com/hexug/devcloud/mcenter/client/rpc"
	"gitee.com/hexug/devcloud/mcenter/test/format"
)

var (
	client *rpc.ClientSet
	ctx    = context.TODO()
)

func init() {
	conf := rpc.NewConfig()
	conf.ClientId = "da4373ba-fc6f-454f-8642-402c933afaf9"
	conf.ClientSecret = "6095939e-c627-43f2-a227-45545dbcf315"
	c, err := rpc.NewClient(conf)
	if err != nil {
		panic(err)
	}
	client = c
}

// 校验令牌
func TestValidateToken(t *testing.T) {
	vtr := token.NewValidateTokenRequest("c")
	vtr.AccessToken = "9dcac454-4d7a-4307-87f6-ff17ae67b2e5"

	tk, err := client.Token().ValidateToken(ctx, vtr)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(format.FormatToJson(tk))
}

// 创建用户
func TestCreateUser(t *testing.T) {
	cur := user.NewCreateUserRequest("b11", "aaaaaaaaaaa")
	user, err := client.User().CreateUser(ctx, cur)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(format.FormatToJson(user))
}

// 查询用户
func TestDetailUser(t *testing.T) {
	dur := user.NewDetailUserRequest()
	dur.Value = "5095555a-633e-48a3-bbe1-7141ceb14b7a"
	user, err := client.User().DetailUser(ctx, dur)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(format.FormatToJson(user))
}

func TestQueryUser(t *testing.T) {
	qur := user.NewQueryUserRequest()
	us, err := client.User().QueryUser(ctx, qur)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(format.FormatToJson(us))
}

func TestUpdateUser(t *testing.T) {
	dur := user.NewUpdateUserRequest()
	dur.Id = "b59e56a0-adb9-40c6-9b38-a9bea874ebb5"
	dur.Username = "b2"
	user, err := client.User().UpdateUser(ctx, dur)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(format.FormatToJson(user))
}
