package rpc

import (
	"context"
	"time"

	"gitee.com/hexug/devcloud/mcenter/apps/service"
	"google.golang.org/grpc/credentials"
)

// 实现了这个中间件
var _ credentials.PerRPCCredentials = (*Config)(nil)

type Config struct {
	TimeoutSecond uint64 `json:"timeout_second"`
	Address       string `json:"address"`
	ClientId      string `json:"client_id"`
	ClientSecret  string `json:"client_secret"`
	ServerID      string `json:"server_id"`
}

func NewConfig() *Config {
	return &Config{
		Address:       "127.0.0.1:12345",
		TimeoutSecond: 10,
	}
}

func (c *Config) Timeout() time.Duration {
	return time.Duration(c.TimeoutSecond) * time.Second
}

// 实现以下接口，用来携带凭证
func (c *Config) GetRequestMetadata(ctx context.Context, uri ...string) (map[string]string, error) {
	return map[string]string{
		service.ClientHeaderKey: c.ClientId,
		service.ClientSecretKey: c.ClientSecret,
	}, nil
}

func (c *Config) RequireTransportSecurity() bool {
	return false
}
