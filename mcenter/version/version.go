package version

import "fmt"

var (
	// GIT_TAG 包的发行版本
	GIT_TAG string
	// GIT_COMMIT 提交代码的commit号
	GIT_COMMIT string
	// GIT_BRANCH 提交代码的分支
	GIT_BRANCH string
	// BUILD_TIME 编译的时间
	BUILD_TIME string
	// GO_VERSION go的版本
	GO_VERSION string
)

// FullVersion 仿照docker
//
//	$ docker version
//	Client:	Docker Engine - Community
//	Version:	20.10.17
//	API version:	1.41
//	Go version:	go1.17.11
//	Git commit:	100c701
//	Built:	Mon Jun  6 23:05:12 2022
//	OS/Arch:	linux/amd64
//	Context:	default
//	Experimental:	true
func FullVersion() string {
	v := fmt.Sprintf("Version   : %s\nBuild Time: %s\nGit Branch: %s\nGit Commit: %s\nGo Version: %s\n", GIT_TAG, BUILD_TIME, GIT_BRANCH, GIT_COMMIT, GO_VERSION)
	return v
}

// ShortVersion 仿照docker
//
//	$ docker -v
//	Docker version 20.10.17, build 100c701
func ShortVersion() string {
	commit := ""
	if len(GIT_COMMIT) > 8 {
		commit = GIT_COMMIT[:8]
	} else {
		commit = GIT_COMMIT
	}
	return fmt.Sprintf("Vblog-api version %s,build %s,%s", GIT_TAG, commit, GO_VERSION)
}
