package impl

import (
	"fmt"

	"gitee.com/hexug/devcloud/mcenter/apps"
	"gitee.com/hexug/devcloud/mcenter/apps/user"
	"gitee.com/hexug/devcloud/mcenter/conf"
	"go.mongodb.org/mongo-driver/mongo"
	"google.golang.org/grpc"
)

// 约束
var _ user.Server = (*implServer)(nil)
var _ user.RPCServer = (*implServer)(nil)
var _ apps.AppObj = (*implServer)(nil)
var _ apps.GrpcObj = (*implServer)(nil)

type implServer struct {
	user.UnimplementedRPCServer
	Col *mongo.Collection
}

var (
	svc = &implServer{}
)

func (i *implServer) Name() string {
	return user.AppName
}
func (i *implServer) Init() error {
	i.Col = conf.DB().MongoClient.Collection(user.AppName)
	if i.Col == nil {
		return fmt.Errorf("数据库 %s 获取失败,疑似尚未注册", user.AppName)
	}
	return nil
}
func (i *implServer) ReagisterGrpc(s *grpc.Server) {
	user.RegisterRPCServer(s, i)
}

func init() {
	apps.RegisterApp(svc)
	apps.RegisterGrpc(svc)
}
