package impl_test

import (
	"context"
	"fmt"
	"testing"

	"gitee.com/hexug/devcloud/mcenter/apps"
	"gitee.com/hexug/devcloud/mcenter/apps/user"
	_ "gitee.com/hexug/devcloud/mcenter/apps/user/impl"
	"gitee.com/hexug/devcloud/mcenter/conf"
	"gitee.com/hexug/devcloud/mcenter/test/format"
)

var (
	svc user.Server
	ctx = context.TODO()
)

func init() {
	conf.LoadConfig("../../../etc/config.toml")
	svc = apps.GetApp(user.AppName).(user.Server)
	apps.GetApp(user.AppName).Init()
}

func TestCreateUser(t *testing.T) {
	cr := user.NewCreateUserRequest(
		"c1", "a",
	)
	user, err := svc.CreateUser(ctx, cr)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(user)
}
func TestUpdateUser(t *testing.T) {
	ur := user.NewUpdateUserRequest()
	ur.Id = "edcf6c18-4946-49bf-be21-92999a7e4b3e"
	// ur.Password = "aaaaaaaaaaaaaaassssssa"
	ur.Domain = "admin"
	u, err := svc.UpdateUser(ctx, ur)
	if err != nil {
		t.Fatal(err)
	}
	fmt.Println(format.FormatToJson(u))
}
func TestDetailUser(t *testing.T) {
	dr := user.NewDetailUserRequestByUsername("c")
	u, err := svc.DetailUser(ctx, dr)
	if err != nil {
		t.Fatal(err)
	}
	fmt.Println(format.FormatToJson(u))
}
func TestDeleteUser(t *testing.T) {
	dr := user.NewDeleteUserRequest()
	dr.DetailType = user.DESCRIBE_BY_USERNAME
	dr.Value = "c"
	u, err := svc.DeleteUser(ctx, dr)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(u)

	fmt.Println(format.FormatToJson(u))
}
func TestQueryUser(t *testing.T) {
	dr := user.NewQueryUserRequest()
	dr.PageSize = 3
	dr.PageNumber = 1
	dr.Keyword = "1"
	u, err := svc.QueryUser(ctx, dr)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(u.Total)
	fmt.Println(format.FormatToJson(u))
	// for _, i := range u.Items {
	// 	fmt.Println(format.FormatToJson(i))
	// }
}
func TestCheckPassword(t *testing.T) {
	dr := user.NewDetailUserRequest()
	dr.DetailType = user.DESCRIBE_BY_USERNAME
	dr.Value = "c"
	u, err := svc.DetailUser(ctx, dr)
	if err != nil {
		t.Fatal(err)
	}
	fmt.Println(u.Spec.Password)
	err = u.CheckPassword("a")
	if err != nil {
		t.Fatal(err)
	}
	// for _, i := range u.Items {
	// 	fmt.Println(format.FormatToJson(i))
	// }
}
