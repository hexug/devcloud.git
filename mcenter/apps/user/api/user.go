package api

import (
	"net/http"
	"strconv"

	"gitee.com/hexug/devcloud/mcenter/apps/user"
	"gitee.com/hexug/devcloud/mcenter/common/logger"
	"github.com/emicklei/go-restful/v3"
)

func (a *ApiServer) CreateUser(request *restful.Request, response *restful.Response) {
	cr := user.NewCreateUserRequestApi()
	if err := request.ReadEntity(cr); err != nil {
		logger.L().Error(err.Error())
		response.WriteErrorString(http.StatusBadRequest, "请求参数有误")
		return
	}
	user, err := a.Svc.CreateUser(request.Request.Context(), cr)
	if err != nil {
		logger.L().Error(err.Error())
		response.WriteErrorString(http.StatusNotExtended, err.Error())
		return
	}
	response.WriteHeaderAndEntity(http.StatusCreated, user)
}
func (a *ApiServer) DeleteUser(request *restful.Request, response *restful.Response) {
	dr := user.NewDeleteUserRequest()
	if err := request.ReadEntity(dr); err != nil {
		logger.L().Error(err.Error())
		response.WriteErrorString(http.StatusBadRequest, "请求参数有误")
		return
	}
	user, err := a.Svc.DeleteUser(request.Request.Context(), dr)
	if err != nil {
		logger.L().Error(err.Error())
		response.WriteErrorString(http.StatusNotFound, err.Error())
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, user)
}
func (a *ApiServer) UpdateUser(request *restful.Request, response *restful.Response) {
	ur := user.NewUpdateUserRequest()
	if err := request.ReadEntity(ur); err != nil {
		logger.L().Error(err.Error())
		response.WriteErrorString(http.StatusBadRequest, "请求参数有误")
		return
	}
	user, err := a.Svc.UpdateUser(request.Request.Context(), ur)

	if err != nil {
		logger.L().Error(err.Error())
		response.WriteErrorString(http.StatusNotFound, err.Error())
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, user)
}
func (a *ApiServer) QueryUser(request *restful.Request, response *restful.Response) {
	qur := user.NewQueryUserRequest()
	// fmt.Printf("初始值%+v\n", qur)
	//抽取pageSize
	pageSize := request.QueryParameter("page_size")
	size, err := strconv.ParseInt(pageSize, 10, 64)
	if err != nil {
		if err == strconv.ErrSyntax {
			logger.L().Info("没有传pageSize")
		} else {
			logger.L().Error(err.Error())
		}
	} else {
		qur.PageSize = size
	}

	//抽取pageNumber
	pageNumber := request.QueryParameter("page_number")
	num, err := strconv.ParseInt(pageNumber, 10, 64)
	if err != nil {
		if err == strconv.ErrSyntax {
			logger.L().Info("没有传pageNumber")
		} else {
			logger.L().Error(err.Error())
		}
	} else {
		qur.PageNumber = num
	}
	//抽取keyword
	qur.Keyword = request.QueryParameter("keyword")
	// fmt.Printf("%+v\n", qur)
	//查询
	us, err := a.Svc.QueryUser(request.Request.Context(), qur)
	if err != nil {
		logger.L().Error(err.Error())
		response.WriteErrorString(http.StatusNotAcceptable, err.Error())
		return
	}
	// response.WriteHeaderAndJson(http.StatusOK, us, restful.MIME_JSON)
	response.WriteAsJson(us)
}
func (a *ApiServer) DetailUser(request *restful.Request, response *restful.Response) {
	// http://127.0.0.1:2345/mcenter/api/v1/users/{id}
	id := request.PathParameter("id")
	dur := user.NewDetailUserRequestById(id)
	u, err := a.Svc.DetailUser(request.Request.Context(), dur)
	if err != nil {
		logger.L().Error(err.Error())
		response.WriteErrorString(http.StatusNotAcceptable, err.Error())
		return
	}
	u.Desensitize()
	response.WriteAsJson(u)
}
