package all

import (
	_ "gitee.com/hexug/devcloud/mcenter/apps/endpoint/impl"
	_ "gitee.com/hexug/devcloud/mcenter/apps/permission/impl"
	_ "gitee.com/hexug/devcloud/mcenter/apps/policy/impl"
	_ "gitee.com/hexug/devcloud/mcenter/apps/role/impl"
	_ "gitee.com/hexug/devcloud/mcenter/apps/service/impl"
	_ "gitee.com/hexug/devcloud/mcenter/apps/token/impl"
	_ "gitee.com/hexug/devcloud/mcenter/apps/user/impl"
)
