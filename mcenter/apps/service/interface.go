package service

import (
	context "context"

	"gitee.com/hexug/devcloud/mcenter/consted"
)

var (
	AppName = "services"
)

type Server interface {
	RPCServer
	// 创建服务
	CreateService(ctx context.Context, in *CreateServiceRequest) (*Service, error)
}

func NewCreateServiceRequest() *CreateServiceRequest {
	return &CreateServiceRequest{
		Domain:    consted.DEFAULT_DOMAIN,
		Namespace: consted.DEFAULT_NAMESPACE,
	}
}

func NewQueryServiceRequest() *QueryServiceRequest {
	return &QueryServiceRequest{
		PageSize:   5,
		PageNumber: 1,
	}
}

func NewDetailServiceRequestByID(id string) *DetailServiceRequest {
	return &DetailServiceRequest{
		DetailType: DESCRIBE_BY_SERVICE_ID,
		Value:      id,
	}
}
func NewDetailServiceRequestByCREDENTAILID(id string) *DetailServiceRequest {
	return &DetailServiceRequest{
		DetailType: DESCRIBE_BY_SERVICE_CREDENTAIL_ID,
		Value:      id,
	}
}
