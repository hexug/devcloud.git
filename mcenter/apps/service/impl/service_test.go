package impl_test

import (
	"context"
	"fmt"
	"testing"

	"gitee.com/hexug/devcloud/mcenter/apps"
	_ "gitee.com/hexug/devcloud/mcenter/apps/all"
	"gitee.com/hexug/devcloud/mcenter/apps/service"
	"gitee.com/hexug/devcloud/mcenter/conf"
	"gitee.com/hexug/devcloud/mcenter/consted"
	"gitee.com/hexug/devcloud/mcenter/test/format"
)

var (
	svc service.Server
	ctx = context.TODO()
)

func init() {
	conf.LoadConfig("../../../etc/config.toml")
	svc = apps.GetApp(service.AppName).(service.Server)
	apps.GetApp(service.AppName).Init()
}

func TestCreateService(t *testing.T) {
	csr := service.NewCreateServiceRequest()
	csr.Name = "maudit"
	csr.Namespace = consted.DEFAULT_NAMESPACE
	serv, err := svc.CreateService(ctx, csr)
	if err != nil {
		t.Fatal(err.Error())
	}
	t.Log(format.FormatToJson(serv))
}
func TestQueryUser(t *testing.T) {
	dr := service.NewQueryServiceRequest()
	// dr.PageSize = 3
	// dr.PageNumber = 1
	// dr.Keyword = "1"
	u, err := svc.QueryService(ctx, dr)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(u.Total)
	fmt.Println(format.FormatToJson(u))
}

func TestDetailService(t *testing.T) {
	dr := service.NewDetailServiceRequestByCREDENTAILID("5f09130d8-f6b7-48ca-81ec-e7d3211802f0")
	u, err := svc.DetailService(ctx, dr)
	if err != nil {
		t.Fatal(err)
	}
	fmt.Println(format.FormatToJson(u))
}
