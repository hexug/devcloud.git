// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.28.1
// 	protoc        v3.21.12
// source: apps/service/pb/service.proto

package service

import (
	meta "gitee.com/hexug/devcloud/mcenter/common/meta"
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

// 查询方式
type DESCRIBE_BY int32

const (
	DESCRIBE_BY_SERVICE_ID            DESCRIBE_BY = 0
	DESCRIBE_BY_SERVICE_CREDENTAIL_ID DESCRIBE_BY = 1
)

// Enum value maps for DESCRIBE_BY.
var (
	DESCRIBE_BY_name = map[int32]string{
		0: "SERVICE_ID",
		1: "SERVICE_CREDENTAIL_ID",
	}
	DESCRIBE_BY_value = map[string]int32{
		"SERVICE_ID":            0,
		"SERVICE_CREDENTAIL_ID": 1,
	}
)

func (x DESCRIBE_BY) Enum() *DESCRIBE_BY {
	p := new(DESCRIBE_BY)
	*p = x
	return p
}

func (x DESCRIBE_BY) String() string {
	return protoimpl.X.EnumStringOf(x.Descriptor(), protoreflect.EnumNumber(x))
}

func (DESCRIBE_BY) Descriptor() protoreflect.EnumDescriptor {
	return file_apps_service_pb_service_proto_enumTypes[0].Descriptor()
}

func (DESCRIBE_BY) Type() protoreflect.EnumType {
	return &file_apps_service_pb_service_proto_enumTypes[0]
}

func (x DESCRIBE_BY) Number() protoreflect.EnumNumber {
	return protoreflect.EnumNumber(x)
}

// Deprecated: Use DESCRIBE_BY.Descriptor instead.
func (DESCRIBE_BY) EnumDescriptor() ([]byte, []int) {
	return file_apps_service_pb_service_proto_rawDescGZIP(), []int{0}
}

type Service struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	// @gotags: bson:",inline" json:"meta"
	Meta *meta.Meta `protobuf:"bytes,1,opt,name=meta,proto3" json:"meta" bson:",inline"`
	// @gotags: bson:",inline" json:"spec"
	Spec *CreateServiceRequest `protobuf:"bytes,2,opt,name=spec,proto3" json:"spec" bson:",inline"`
	// 服务凭证, 用于校验服务合法性
	// @gotags: bson:"credentail" json:"credentail"
	Credentail *Credentail `protobuf:"bytes,3,opt,name=credentail,proto3" json:"credentail" bson:"credentail"`
}

func (x *Service) Reset() {
	*x = Service{}
	if protoimpl.UnsafeEnabled {
		mi := &file_apps_service_pb_service_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Service) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Service) ProtoMessage() {}

func (x *Service) ProtoReflect() protoreflect.Message {
	mi := &file_apps_service_pb_service_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Service.ProtoReflect.Descriptor instead.
func (*Service) Descriptor() ([]byte, []int) {
	return file_apps_service_pb_service_proto_rawDescGZIP(), []int{0}
}

func (x *Service) GetMeta() *meta.Meta {
	if x != nil {
		return x.Meta
	}
	return nil
}

func (x *Service) GetSpec() *CreateServiceRequest {
	if x != nil {
		return x.Spec
	}
	return nil
}

func (x *Service) GetCredentail() *Credentail {
	if x != nil {
		return x.Credentail
	}
	return nil
}

type ServiceSet struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	// 总用户数
	// @gotags: bson:"total" json:"total" validate:"required"
	Total int64 `protobuf:"varint,1,opt,name=total,proto3" json:"total" bson:"total" validate:"required"`
	// @gotags: bson:"items" json:"items" validate:"required"
	Items []*Service `protobuf:"bytes,2,rep,name=items,proto3" json:"items" bson:"items" validate:"required"`
}

func (x *ServiceSet) Reset() {
	*x = ServiceSet{}
	if protoimpl.UnsafeEnabled {
		mi := &file_apps_service_pb_service_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *ServiceSet) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*ServiceSet) ProtoMessage() {}

func (x *ServiceSet) ProtoReflect() protoreflect.Message {
	mi := &file_apps_service_pb_service_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use ServiceSet.ProtoReflect.Descriptor instead.
func (*ServiceSet) Descriptor() ([]byte, []int) {
	return file_apps_service_pb_service_proto_rawDescGZIP(), []int{1}
}

func (x *ServiceSet) GetTotal() int64 {
	if x != nil {
		return x.Total
	}
	return 0
}

func (x *ServiceSet) GetItems() []*Service {
	if x != nil {
		return x.Items
	}
	return nil
}

type CreateServiceRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	// 用户所属的Domain(公司或者组织名字)
	// @gotags: bson:"domain" json:"domain" validate:"required"
	Domain string `protobuf:"bytes,1,opt,name=domain,proto3" json:"domain" bson:"domain" validate:"required"`
	// 服务所属空间(项目)
	// @gotags: bson:"namespace" json:"namespace" validate:"required"
	Namespace string `protobuf:"bytes,2,opt,name=namespace,proto3" json:"namespace" bson:"namespace" validate:"required"`
	// 服务名称
	// @gotags: bson:"name" json:"name" validate:"required"
	Name string `protobuf:"bytes,3,opt,name=name,proto3" json:"name" bson:"name" validate:"required"`
}

func (x *CreateServiceRequest) Reset() {
	*x = CreateServiceRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_apps_service_pb_service_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *CreateServiceRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*CreateServiceRequest) ProtoMessage() {}

func (x *CreateServiceRequest) ProtoReflect() protoreflect.Message {
	mi := &file_apps_service_pb_service_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use CreateServiceRequest.ProtoReflect.Descriptor instead.
func (*CreateServiceRequest) Descriptor() ([]byte, []int) {
	return file_apps_service_pb_service_proto_rawDescGZIP(), []int{2}
}

func (x *CreateServiceRequest) GetDomain() string {
	if x != nil {
		return x.Domain
	}
	return ""
}

func (x *CreateServiceRequest) GetNamespace() string {
	if x != nil {
		return x.Namespace
	}
	return ""
}

func (x *CreateServiceRequest) GetName() string {
	if x != nil {
		return x.Name
	}
	return ""
}

type Credentail struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	// 凭证的ID
	// @gotags: bson:"client_id" json:"client_id"
	ClientId string `protobuf:"bytes,1,opt,name=client_id,json=clientId,proto3" json:"client_id" bson:"client_id"`
	// 凭证
	// @gotags: bson:"client_secret" json:"client_secret"
	ClientSecret string `protobuf:"bytes,2,opt,name=client_secret,json=clientSecret,proto3" json:"client_secret" bson:"client_secret"`
}

func (x *Credentail) Reset() {
	*x = Credentail{}
	if protoimpl.UnsafeEnabled {
		mi := &file_apps_service_pb_service_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Credentail) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Credentail) ProtoMessage() {}

func (x *Credentail) ProtoReflect() protoreflect.Message {
	mi := &file_apps_service_pb_service_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Credentail.ProtoReflect.Descriptor instead.
func (*Credentail) Descriptor() ([]byte, []int) {
	return file_apps_service_pb_service_proto_rawDescGZIP(), []int{3}
}

func (x *Credentail) GetClientId() string {
	if x != nil {
		return x.ClientId
	}
	return ""
}

func (x *Credentail) GetClientSecret() string {
	if x != nil {
		return x.ClientSecret
	}
	return ""
}

// 查询，必须给分页
type QueryServiceRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	// @gotags: bson:"page_size" json:"page_size" validate:"required"
	PageSize int64 `protobuf:"varint,1,opt,name=page_size,json=pageSize,proto3" json:"page_size" bson:"page_size" validate:"required"`
	// @gotags: bson:"page_number" json:"page_number" validate:"required"
	PageNumber int64 `protobuf:"varint,2,opt,name=page_number,json=pageNumber,proto3" json:"page_number" bson:"page_number" validate:"required"`
	// @gotags: bson:"keyword" json:"keyword"
	Keyword string `protobuf:"bytes,3,opt,name=keyword,proto3" json:"keyword" bson:"keyword"`
}

func (x *QueryServiceRequest) Reset() {
	*x = QueryServiceRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_apps_service_pb_service_proto_msgTypes[4]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *QueryServiceRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*QueryServiceRequest) ProtoMessage() {}

func (x *QueryServiceRequest) ProtoReflect() protoreflect.Message {
	mi := &file_apps_service_pb_service_proto_msgTypes[4]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use QueryServiceRequest.ProtoReflect.Descriptor instead.
func (*QueryServiceRequest) Descriptor() ([]byte, []int) {
	return file_apps_service_pb_service_proto_rawDescGZIP(), []int{4}
}

func (x *QueryServiceRequest) GetPageSize() int64 {
	if x != nil {
		return x.PageSize
	}
	return 0
}

func (x *QueryServiceRequest) GetPageNumber() int64 {
	if x != nil {
		return x.PageNumber
	}
	return 0
}

func (x *QueryServiceRequest) GetKeyword() string {
	if x != nil {
		return x.Keyword
	}
	return ""
}

// 默认按照ID查询
type DetailServiceRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	// 查询类型
	// @gotags: bson:"detail_type" json:"detail_type"
	DetailType DESCRIBE_BY `protobuf:"varint,1,opt,name=detail_type,json=detailType,proto3,enum=devcloud.service.DESCRIBE_BY" json:"detail_type" bson:"detail_type"`
	// 查询字符串
	// @gotags: bson:"value" json:"value" validate:"required"
	Value string `protobuf:"bytes,2,opt,name=value,proto3" json:"value" bson:"value" validate:"required"`
}

func (x *DetailServiceRequest) Reset() {
	*x = DetailServiceRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_apps_service_pb_service_proto_msgTypes[5]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *DetailServiceRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*DetailServiceRequest) ProtoMessage() {}

func (x *DetailServiceRequest) ProtoReflect() protoreflect.Message {
	mi := &file_apps_service_pb_service_proto_msgTypes[5]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use DetailServiceRequest.ProtoReflect.Descriptor instead.
func (*DetailServiceRequest) Descriptor() ([]byte, []int) {
	return file_apps_service_pb_service_proto_rawDescGZIP(), []int{5}
}

func (x *DetailServiceRequest) GetDetailType() DESCRIBE_BY {
	if x != nil {
		return x.DetailType
	}
	return DESCRIBE_BY_SERVICE_ID
}

func (x *DetailServiceRequest) GetValue() string {
	if x != nil {
		return x.Value
	}
	return ""
}

var File_apps_service_pb_service_proto protoreflect.FileDescriptor

var file_apps_service_pb_service_proto_rawDesc = []byte{
	0x0a, 0x1d, 0x61, 0x70, 0x70, 0x73, 0x2f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2f, 0x70,
	0x62, 0x2f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12,
	0x10, 0x64, 0x65, 0x76, 0x63, 0x6c, 0x6f, 0x75, 0x64, 0x2e, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63,
	0x65, 0x1a, 0x16, 0x63, 0x6f, 0x6d, 0x6d, 0x6f, 0x6e, 0x2f, 0x6d, 0x65, 0x74, 0x61, 0x2f, 0x6d,
	0x65, 0x74, 0x61, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x22, 0xac, 0x01, 0x0a, 0x07, 0x53, 0x65,
	0x72, 0x76, 0x69, 0x63, 0x65, 0x12, 0x27, 0x0a, 0x04, 0x6d, 0x65, 0x74, 0x61, 0x18, 0x01, 0x20,
	0x01, 0x28, 0x0b, 0x32, 0x13, 0x2e, 0x64, 0x65, 0x76, 0x63, 0x6c, 0x6f, 0x75, 0x64, 0x2e, 0x6d,
	0x65, 0x74, 0x61, 0x2e, 0x4d, 0x65, 0x74, 0x61, 0x52, 0x04, 0x6d, 0x65, 0x74, 0x61, 0x12, 0x3a,
	0x0a, 0x04, 0x73, 0x70, 0x65, 0x63, 0x18, 0x02, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x26, 0x2e, 0x64,
	0x65, 0x76, 0x63, 0x6c, 0x6f, 0x75, 0x64, 0x2e, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e,
	0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x53, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x52, 0x65, 0x71,
	0x75, 0x65, 0x73, 0x74, 0x52, 0x04, 0x73, 0x70, 0x65, 0x63, 0x12, 0x3c, 0x0a, 0x0a, 0x63, 0x72,
	0x65, 0x64, 0x65, 0x6e, 0x74, 0x61, 0x69, 0x6c, 0x18, 0x03, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x1c,
	0x2e, 0x64, 0x65, 0x76, 0x63, 0x6c, 0x6f, 0x75, 0x64, 0x2e, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63,
	0x65, 0x2e, 0x43, 0x72, 0x65, 0x64, 0x65, 0x6e, 0x74, 0x61, 0x69, 0x6c, 0x52, 0x0a, 0x63, 0x72,
	0x65, 0x64, 0x65, 0x6e, 0x74, 0x61, 0x69, 0x6c, 0x22, 0x53, 0x0a, 0x0a, 0x53, 0x65, 0x72, 0x76,
	0x69, 0x63, 0x65, 0x53, 0x65, 0x74, 0x12, 0x14, 0x0a, 0x05, 0x74, 0x6f, 0x74, 0x61, 0x6c, 0x18,
	0x01, 0x20, 0x01, 0x28, 0x03, 0x52, 0x05, 0x74, 0x6f, 0x74, 0x61, 0x6c, 0x12, 0x2f, 0x0a, 0x05,
	0x69, 0x74, 0x65, 0x6d, 0x73, 0x18, 0x02, 0x20, 0x03, 0x28, 0x0b, 0x32, 0x19, 0x2e, 0x64, 0x65,
	0x76, 0x63, 0x6c, 0x6f, 0x75, 0x64, 0x2e, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x53,
	0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x52, 0x05, 0x69, 0x74, 0x65, 0x6d, 0x73, 0x22, 0x60, 0x0a,
	0x14, 0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x53, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x52, 0x65,
	0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x16, 0x0a, 0x06, 0x64, 0x6f, 0x6d, 0x61, 0x69, 0x6e, 0x18,
	0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x06, 0x64, 0x6f, 0x6d, 0x61, 0x69, 0x6e, 0x12, 0x1c, 0x0a,
	0x09, 0x6e, 0x61, 0x6d, 0x65, 0x73, 0x70, 0x61, 0x63, 0x65, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09,
	0x52, 0x09, 0x6e, 0x61, 0x6d, 0x65, 0x73, 0x70, 0x61, 0x63, 0x65, 0x12, 0x12, 0x0a, 0x04, 0x6e,
	0x61, 0x6d, 0x65, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x04, 0x6e, 0x61, 0x6d, 0x65, 0x22,
	0x4e, 0x0a, 0x0a, 0x43, 0x72, 0x65, 0x64, 0x65, 0x6e, 0x74, 0x61, 0x69, 0x6c, 0x12, 0x1b, 0x0a,
	0x09, 0x63, 0x6c, 0x69, 0x65, 0x6e, 0x74, 0x5f, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09,
	0x52, 0x08, 0x63, 0x6c, 0x69, 0x65, 0x6e, 0x74, 0x49, 0x64, 0x12, 0x23, 0x0a, 0x0d, 0x63, 0x6c,
	0x69, 0x65, 0x6e, 0x74, 0x5f, 0x73, 0x65, 0x63, 0x72, 0x65, 0x74, 0x18, 0x02, 0x20, 0x01, 0x28,
	0x09, 0x52, 0x0c, 0x63, 0x6c, 0x69, 0x65, 0x6e, 0x74, 0x53, 0x65, 0x63, 0x72, 0x65, 0x74, 0x22,
	0x6d, 0x0a, 0x13, 0x51, 0x75, 0x65, 0x72, 0x79, 0x53, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x52,
	0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x1b, 0x0a, 0x09, 0x70, 0x61, 0x67, 0x65, 0x5f, 0x73,
	0x69, 0x7a, 0x65, 0x18, 0x01, 0x20, 0x01, 0x28, 0x03, 0x52, 0x08, 0x70, 0x61, 0x67, 0x65, 0x53,
	0x69, 0x7a, 0x65, 0x12, 0x1f, 0x0a, 0x0b, 0x70, 0x61, 0x67, 0x65, 0x5f, 0x6e, 0x75, 0x6d, 0x62,
	0x65, 0x72, 0x18, 0x02, 0x20, 0x01, 0x28, 0x03, 0x52, 0x0a, 0x70, 0x61, 0x67, 0x65, 0x4e, 0x75,
	0x6d, 0x62, 0x65, 0x72, 0x12, 0x18, 0x0a, 0x07, 0x6b, 0x65, 0x79, 0x77, 0x6f, 0x72, 0x64, 0x18,
	0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x07, 0x6b, 0x65, 0x79, 0x77, 0x6f, 0x72, 0x64, 0x22, 0x6c,
	0x0a, 0x14, 0x44, 0x65, 0x74, 0x61, 0x69, 0x6c, 0x53, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x52,
	0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x3e, 0x0a, 0x0b, 0x64, 0x65, 0x74, 0x61, 0x69, 0x6c,
	0x5f, 0x74, 0x79, 0x70, 0x65, 0x18, 0x01, 0x20, 0x01, 0x28, 0x0e, 0x32, 0x1d, 0x2e, 0x64, 0x65,
	0x76, 0x63, 0x6c, 0x6f, 0x75, 0x64, 0x2e, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x44,
	0x45, 0x53, 0x43, 0x52, 0x49, 0x42, 0x45, 0x5f, 0x42, 0x59, 0x52, 0x0a, 0x64, 0x65, 0x74, 0x61,
	0x69, 0x6c, 0x54, 0x79, 0x70, 0x65, 0x12, 0x14, 0x0a, 0x05, 0x76, 0x61, 0x6c, 0x75, 0x65, 0x18,
	0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x05, 0x76, 0x61, 0x6c, 0x75, 0x65, 0x2a, 0x38, 0x0a, 0x0b,
	0x44, 0x45, 0x53, 0x43, 0x52, 0x49, 0x42, 0x45, 0x5f, 0x42, 0x59, 0x12, 0x0e, 0x0a, 0x0a, 0x53,
	0x45, 0x52, 0x56, 0x49, 0x43, 0x45, 0x5f, 0x49, 0x44, 0x10, 0x00, 0x12, 0x19, 0x0a, 0x15, 0x53,
	0x45, 0x52, 0x56, 0x49, 0x43, 0x45, 0x5f, 0x43, 0x52, 0x45, 0x44, 0x45, 0x4e, 0x54, 0x41, 0x49,
	0x4c, 0x5f, 0x49, 0x44, 0x10, 0x01, 0x42, 0x2f, 0x5a, 0x2d, 0x67, 0x69, 0x74, 0x65, 0x65, 0x2e,
	0x63, 0x6f, 0x6d, 0x2f, 0x68, 0x65, 0x78, 0x75, 0x67, 0x2f, 0x64, 0x65, 0x76, 0x63, 0x6c, 0x6f,
	0x75, 0x64, 0x2f, 0x6d, 0x63, 0x65, 0x6e, 0x74, 0x65, 0x72, 0x2f, 0x61, 0x70, 0x70, 0x73, 0x2f,
	0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_apps_service_pb_service_proto_rawDescOnce sync.Once
	file_apps_service_pb_service_proto_rawDescData = file_apps_service_pb_service_proto_rawDesc
)

func file_apps_service_pb_service_proto_rawDescGZIP() []byte {
	file_apps_service_pb_service_proto_rawDescOnce.Do(func() {
		file_apps_service_pb_service_proto_rawDescData = protoimpl.X.CompressGZIP(file_apps_service_pb_service_proto_rawDescData)
	})
	return file_apps_service_pb_service_proto_rawDescData
}

var file_apps_service_pb_service_proto_enumTypes = make([]protoimpl.EnumInfo, 1)
var file_apps_service_pb_service_proto_msgTypes = make([]protoimpl.MessageInfo, 6)
var file_apps_service_pb_service_proto_goTypes = []interface{}{
	(DESCRIBE_BY)(0),             // 0: devcloud.service.DESCRIBE_BY
	(*Service)(nil),              // 1: devcloud.service.Service
	(*ServiceSet)(nil),           // 2: devcloud.service.ServiceSet
	(*CreateServiceRequest)(nil), // 3: devcloud.service.CreateServiceRequest
	(*Credentail)(nil),           // 4: devcloud.service.Credentail
	(*QueryServiceRequest)(nil),  // 5: devcloud.service.QueryServiceRequest
	(*DetailServiceRequest)(nil), // 6: devcloud.service.DetailServiceRequest
	(*meta.Meta)(nil),            // 7: devcloud.meta.Meta
}
var file_apps_service_pb_service_proto_depIdxs = []int32{
	7, // 0: devcloud.service.Service.meta:type_name -> devcloud.meta.Meta
	3, // 1: devcloud.service.Service.spec:type_name -> devcloud.service.CreateServiceRequest
	4, // 2: devcloud.service.Service.credentail:type_name -> devcloud.service.Credentail
	1, // 3: devcloud.service.ServiceSet.items:type_name -> devcloud.service.Service
	0, // 4: devcloud.service.DetailServiceRequest.detail_type:type_name -> devcloud.service.DESCRIBE_BY
	5, // [5:5] is the sub-list for method output_type
	5, // [5:5] is the sub-list for method input_type
	5, // [5:5] is the sub-list for extension type_name
	5, // [5:5] is the sub-list for extension extendee
	0, // [0:5] is the sub-list for field type_name
}

func init() { file_apps_service_pb_service_proto_init() }
func file_apps_service_pb_service_proto_init() {
	if File_apps_service_pb_service_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_apps_service_pb_service_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Service); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_apps_service_pb_service_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*ServiceSet); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_apps_service_pb_service_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*CreateServiceRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_apps_service_pb_service_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Credentail); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_apps_service_pb_service_proto_msgTypes[4].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*QueryServiceRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_apps_service_pb_service_proto_msgTypes[5].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*DetailServiceRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_apps_service_pb_service_proto_rawDesc,
			NumEnums:      1,
			NumMessages:   6,
			NumExtensions: 0,
			NumServices:   0,
		},
		GoTypes:           file_apps_service_pb_service_proto_goTypes,
		DependencyIndexes: file_apps_service_pb_service_proto_depIdxs,
		EnumInfos:         file_apps_service_pb_service_proto_enumTypes,
		MessageInfos:      file_apps_service_pb_service_proto_msgTypes,
	}.Build()
	File_apps_service_pb_service_proto = out.File
	file_apps_service_pb_service_proto_rawDesc = nil
	file_apps_service_pb_service_proto_goTypes = nil
	file_apps_service_pb_service_proto_depIdxs = nil
}
