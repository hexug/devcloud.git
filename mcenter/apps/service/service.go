package service

import (
	"gitee.com/hexug/devcloud/mcenter/common/meta"
	"github.com/google/uuid"
)

func NewService(in *CreateServiceRequest) *Service {
	return &Service{
		Meta: meta.NewMeta(),
		Spec: in,
		Credentail: &Credentail{
			ClientId:     uuid.New().String(),
			ClientSecret: uuid.New().String(),
		},
	}
}

func NewServiceSet() *ServiceSet {
	return &ServiceSet{}
}
