package impl_test

import (
	"context"
	"errors"
	"testing"

	"gitee.com/hexug/devcloud/mcenter/apps"
	_ "gitee.com/hexug/devcloud/mcenter/apps/all"
	"gitee.com/hexug/devcloud/mcenter/apps/permission"
	"gitee.com/hexug/devcloud/mcenter/apps/policy"
	"gitee.com/hexug/devcloud/mcenter/apps/role"
	"gitee.com/hexug/devcloud/mcenter/conf"
	"gitee.com/hexug/devcloud/mcenter/test/format"
)

var (
	svc permission.Server
	ctx = context.TODO()
)

func init() {
	conf.LoadConfig("../../../etc/config.toml")
	svc = apps.GetApp(permission.AppName).(permission.Server)
	apps.GetApp(permission.AppName).Init()
	apps.GetApp(policy.AppName).Init()
	apps.GetApp(role.AppName).Init()
}

func TestCheckPermission(t *testing.T) {
	cpmrq := permission.NewCheckPermissionRequest()
	cpmrq.UserId = "445cac57-b419-4b40-8935-d863c6677f32"
	cpmrq.Namespace = "default"
	cpmrq.ServiceId = "315a77d1-8b7e-48e0-8060-6c0c0004dd0d"
	cpmrq.HttpMethod = "post"
	cpmrq.HttpPath = "/data/aa"
	cpmrp, err := svc.CheckPermission(ctx, cpmrq)
	if err != nil {
		t.Fatal(err.Error())
	}
	if !cpmrp.HasPermisson {
		t.Fatal(errors.New("该角色尚无权限访问"))
	}
	t.Log(format.FormatToJson(cpmrp.Policy))
}
