package impl

import (
	"gitee.com/hexug/devcloud/mcenter/apps"
	"gitee.com/hexug/devcloud/mcenter/apps/permission"
	"gitee.com/hexug/devcloud/mcenter/apps/policy"
	"google.golang.org/grpc"
)

// 约束
var _ permission.Server = (*implServer)(nil)
var _ permission.RPCServer = (*implServer)(nil)
var _ apps.AppObj = (*implServer)(nil)
var _ apps.GrpcObj = (*implServer)(nil)

type implServer struct {
	permission.UnimplementedRPCServer
	p policy.Server
}

var (
	svc = &implServer{}
)

func (i *implServer) Name() string {
	return permission.AppName
}
func (i *implServer) Init() error {
	i.p = apps.GetApp(policy.AppName).(policy.Server)
	return nil
}
func (i *implServer) ReagisterGrpc(s *grpc.Server) {
	permission.RegisterRPCServer(s, i)
}

func init() {
	apps.RegisterApp(svc)
	apps.RegisterGrpc(svc)
}
