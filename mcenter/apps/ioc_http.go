package apps

import (
	"errors"
	"fmt"

	"github.com/emicklei/go-restful/v3"
)

var (
	HttpObjLib = map[string]HttpObj{}
)

type HttpObj interface {
	AppObj
	ReagisterRoute(*restful.WebService)
	Version() string
}

func RegisterHttp(obj HttpObj) {
	if _, ok := HttpObjLib[obj.Name()]; ok {
		panic(errors.New("http " + obj.Name() + " 已注册过！"))
	} else {
		HttpObjLib[obj.Name()] = obj
		fmt.Printf("http %s 注册成功。\n", obj.Name())
	}
}

func GetHttp(name string) HttpObj {
	if value, ok := HttpObjLib[name]; ok {
		return value
	} else {
		panic(errors.New("Http" + name + "尚未注册！"))
	}
}
