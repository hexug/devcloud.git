package impl

import (
	"fmt"

	"gitee.com/hexug/devcloud/mcenter/apps"
	"gitee.com/hexug/devcloud/mcenter/apps/endpoint"
	"gitee.com/hexug/devcloud/mcenter/apps/role"
	"gitee.com/hexug/devcloud/mcenter/conf"
	"go.mongodb.org/mongo-driver/mongo"
	"google.golang.org/grpc"
)

// 约束
var _ role.Server = (*implServer)(nil)
var _ role.RPCServer = (*implServer)(nil)
var _ apps.AppObj = (*implServer)(nil)
var _ apps.GrpcObj = (*implServer)(nil)

type implServer struct {
	role.UnimplementedRPCServer
	Col      *mongo.Collection
	endpoint endpoint.Server
}

var (
	svc = &implServer{}
)

func (i *implServer) Name() string {
	return role.AppName
}
func (i *implServer) Init() error {
	i.Col = conf.DB().MongoClient.Collection(role.AppName)
	if i.Col == nil {
		return fmt.Errorf("数据库 %s 获取失败,疑似尚未注册", role.AppName)
	}
	i.endpoint = apps.GetApp(endpoint.AppName).(endpoint.Server)
	return nil
}
func (i *implServer) ReagisterGrpc(s *grpc.Server) {
	role.RegisterRPCServer(s, i)
}

func init() {
	apps.RegisterApp(svc)
	apps.RegisterGrpc(svc)
}
