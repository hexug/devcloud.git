package impl_test

import (
	"context"
	"testing"

	"gitee.com/hexug/devcloud/mcenter/apps"
	_ "gitee.com/hexug/devcloud/mcenter/apps/all"
	"gitee.com/hexug/devcloud/mcenter/apps/endpoint"
	"gitee.com/hexug/devcloud/mcenter/apps/role"
	"gitee.com/hexug/devcloud/mcenter/conf"
	"gitee.com/hexug/devcloud/mcenter/test/format"
)

var (
	svc role.Server
	ctx = context.TODO()
)

func init() {
	conf.LoadConfig("../../../etc/config.toml")
	svc = apps.GetApp(role.AppName).(role.Server)
	apps.GetApp(role.AppName).Init()
	apps.GetApp(endpoint.AppName).Init()
}

func TestCreateRole(t *testing.T) {
	crr := role.NewCreateRoleRequest()
	crr.Name = "admin"
	f := role.NewFeature()
	f.ServiceId = "315a77d1-8b7e-48e0-8060-6c0c0004dd0d"
	f.HttpMethod = "GET"
	f.HttPath = "/mpaas/api/v1/cluster"
	crr.AddFeature(f)
	r, err := svc.CreateRole(ctx, crr)
	if err != nil {
		t.Fatal(err.Error())
	}
	t.Log(format.FormatToJson(r))
}
func TestQueryRole(t *testing.T) {
	crr := role.NewQueryRoleRequest()
	r, err := svc.QueryRole(ctx, crr)
	if err != nil {
		t.Fatal(err.Error())
	}
	t.Log(format.FormatToJson(r))
}
func TestDetailRole(t *testing.T) {
	crr := role.NewDetailRoleRequestByID("69161024-29b9-489d-a3d6-75756656874d")
	r, err := svc.DetailRole(ctx, crr)
	if err != nil {
		t.Fatal(err.Error())
	}
	t.Log(format.FormatToJson(r))
}
