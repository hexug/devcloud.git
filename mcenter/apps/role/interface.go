package role

import context "context"

var (
	AppName = "roles"
)

type Server interface {
	RPCServer
	CreateRole(context.Context, *CreateRoleRequest) (*Role, error)
}
