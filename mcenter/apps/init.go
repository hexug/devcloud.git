package apps

import (
	"fmt"
	"strings"

	"github.com/emicklei/go-restful/v3"
	"google.golang.org/grpc"
)

func InitedHttp() (apis []string) {
	for api := range HttpObjLib {
		apis = append(apis, api)
	}
	return apis
}
func InitedApp() (apps []string) {
	for app := range AppObjLib {
		apps = append(apps, app)
	}
	return apps
}
func InitedGrpc() (grpcs []string) {
	for grpc := range GrpcObjLib {
		grpcs = append(grpcs, grpc)
	}
	return grpcs
}

// 初始化app
func InitApp() {
	for _, obj := range AppObjLib {
		if err := obj.Init(); err != nil {
			panic(err)
		}
	}
}

// 初始化http
func InitHttp(pathPrefix string, r *restful.Container) {
	pathPrefix = strings.Trim(pathPrefix, "/")
	for _, obj := range HttpObjLib {
		if err := obj.Init(); err != nil {
			panic(err)
		}
		ws := new(restful.WebService)
		ws.Path(fmt.Sprintf("/%s/%s/%s", pathPrefix, obj.Version(), obj.Name())).
			//设置可以用来解析的格式
			Consumes(restful.MIME_JSON, restful.MIME_XML).
			//设置返回数据的格式
			Produces(restful.MIME_JSON, restful.MIME_XML) // you can specify this per route as well
		obj.ReagisterRoute(ws)
		r.Add(ws)
	}
}

// 初始化Grpc
func InitGrpc(r *grpc.Server) {
	for _, obj := range GrpcObjLib {
		if err := obj.Init(); err != nil {
			panic(err)
		}
		obj.ReagisterGrpc(r)
	}
}
