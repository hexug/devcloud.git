package policy

import context "context"

var (
	AppName = "policies"
)

type Server interface {
	RPCServer
	// 创建策略
	CreatePolicy(context.Context, *CreatePolicyRequest) (*Policy, error)
}
