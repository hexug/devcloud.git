package impl_test

import (
	"context"
	"testing"

	"gitee.com/hexug/devcloud/mcenter/apps"
	_ "gitee.com/hexug/devcloud/mcenter/apps/all"
	"gitee.com/hexug/devcloud/mcenter/apps/policy"
	"gitee.com/hexug/devcloud/mcenter/apps/role"
	"gitee.com/hexug/devcloud/mcenter/apps/user"
	"gitee.com/hexug/devcloud/mcenter/conf"
	"gitee.com/hexug/devcloud/mcenter/test/format"
)

var (
	svc policy.Server
	ctx = context.TODO()
)

func init() {
	conf.LoadConfig("../../../etc/config.toml")
	svc = apps.GetApp(policy.AppName).(policy.Server)
	apps.GetApp(policy.AppName).Init()
	apps.GetApp(role.AppName).Init()
	apps.GetApp(user.AppName).Init()

}

func TestCreatePolicy(t *testing.T) {
	cpr := policy.NewCreatePolicyRequest()
	cpr.Namespces = "default"
	cpr.RoleId = "69161024-29b9-489d-a3d6-75756656874d"
	cpr.UserId = "445cac57-b419-4b40-8935-d863c6677f32"
	pli, err := svc.CreatePolicy(ctx, cpr)
	if err != nil {
		t.Fatal(err.Error())
	}
	t.Log(format.FormatToJson(pli))
}

func TestQueryPolicy(t *testing.T) {
	qpr := policy.NewQueryPolicyRequest()
	qpr.WithRole = true
	ps, err := svc.QueryPolicy(ctx, qpr)
	if err != nil {
		t.Fatal(err.Error())
	}

	t.Log(format.FormatToJson(ps))
}

func TestDetailPolicy(t *testing.T) {
	DPR := policy.NewDetailPolicyRequest("616b671c-7a61-415e-971d-9a33645c0868")
	pli, err := svc.DetailPolicy(ctx, DPR)
	if err != nil {
		t.Fatal(err.Error())
	}
	t.Log(format.FormatToJson(pli))
}
