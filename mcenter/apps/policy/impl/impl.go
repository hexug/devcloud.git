package impl

import (
	"fmt"

	"gitee.com/hexug/devcloud/mcenter/apps"
	"gitee.com/hexug/devcloud/mcenter/apps/policy"
	"gitee.com/hexug/devcloud/mcenter/apps/role"
	"gitee.com/hexug/devcloud/mcenter/apps/user"
	"gitee.com/hexug/devcloud/mcenter/conf"
	"go.mongodb.org/mongo-driver/mongo"
	"google.golang.org/grpc"
)

// 约束
var _ policy.Server = (*implServer)(nil)
var _ policy.RPCServer = (*implServer)(nil)
var _ apps.AppObj = (*implServer)(nil)
var _ apps.GrpcObj = (*implServer)(nil)

type implServer struct {
	policy.UnimplementedRPCServer
	Col  *mongo.Collection
	RSvc role.Server
	user user.Server
}

var (
	svc = &implServer{}
)

func (i *implServer) Name() string {
	return policy.AppName
}
func (i *implServer) Init() error {
	i.Col = conf.DB().MongoClient.Collection(policy.AppName)
	if i.Col == nil {
		return fmt.Errorf("数据库 %s 获取失败,疑似尚未注册", policy.AppName)
	}
	i.RSvc = apps.GetApp(role.AppName).(role.Server)
	i.user = apps.GetApp(user.AppName).(user.Server)
	return nil
}
func (i *implServer) ReagisterGrpc(s *grpc.Server) {
	policy.RegisterRPCServer(s, i)
}

func init() {
	apps.RegisterApp(svc)
	apps.RegisterGrpc(svc)
}
