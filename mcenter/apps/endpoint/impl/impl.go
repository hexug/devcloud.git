package impl

import (
	"fmt"

	"gitee.com/hexug/devcloud/mcenter/apps"
	"gitee.com/hexug/devcloud/mcenter/apps/endpoint"
	"gitee.com/hexug/devcloud/mcenter/apps/service"
	"gitee.com/hexug/devcloud/mcenter/conf"
	"go.mongodb.org/mongo-driver/mongo"
	"google.golang.org/grpc"
)

// 约束
var _ endpoint.RPCServer = (*implServer)(nil)
var _ apps.AppObj = (*implServer)(nil)
var _ apps.GrpcObj = (*implServer)(nil)

type implServer struct {
	endpoint.UnimplementedRPCServer
	Col     *mongo.Collection
	service service.Server
}

var (
	svc = &implServer{}
)

func (i *implServer) Name() string {
	return endpoint.AppName
}
func (i *implServer) Init() error {
	i.Col = conf.DB().MongoClient.Collection(endpoint.AppName)
	if i.Col == nil {
		return fmt.Errorf("数据库 %s 获取失败,疑似尚未注册", endpoint.AppName)
	}
	i.service = apps.GetApp(service.AppName).(service.Server)
	return nil
}
func (i *implServer) ReagisterGrpc(s *grpc.Server) {
	endpoint.RegisterRPCServer(s, i)
}

func init() {
	apps.RegisterApp(svc)
	apps.RegisterGrpc(svc)
}
