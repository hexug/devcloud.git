package impl_test

import (
	"context"
	"testing"

	"gitee.com/hexug/devcloud/mcenter/apps"
	_ "gitee.com/hexug/devcloud/mcenter/apps/all"
	"gitee.com/hexug/devcloud/mcenter/apps/endpoint"
	"gitee.com/hexug/devcloud/mcenter/apps/service"
	"gitee.com/hexug/devcloud/mcenter/conf"
	"gitee.com/hexug/devcloud/mcenter/test/format"
)

var (
	svc endpoint.Server
	ctx = context.TODO()
)

func init() {
	conf.LoadConfig("../../../etc/config.toml")
	svc = apps.GetApp(endpoint.AppName).(endpoint.Server)
	apps.GetApp(endpoint.AppName).Init()
	apps.GetApp(service.AppName).Init()
}

func TestRegisterEndpoint(t *testing.T) {
	cepr := endpoint.NewCreateEndpointRequest()
	cepr.ServiceId = "fdf27a00-a616-49cf-8e1b5-2fef9e866511"
	cepr.Method = "get"
	cepr.Path = "/data/aa"
	cepr.Auth = false
	rr := endpoint.NewRegisterRequest()
	rr.Items = append(rr.Items, cepr)
	cepr = endpoint.NewCreateEndpointRequest()
	cepr.ServiceId = "315a77d1-8b7e-48e0-8060-6c0c0004dd0d"
	cepr.Method = "post"
	cepr.Path = "/data/aa"
	cepr.Auth = false
	rr.Items = append(rr.Items, cepr)
	ed, err := svc.RegisterEndpoint(ctx, rr)
	if err != nil {
		t.Fatal(err.Error())
	}
	t.Log(ed)
}
func TestQueryEndpoint(t *testing.T) {
	cepr := endpoint.NewQueryEndpointRequest()
	// cepr.PageSize = 1
	// cepr.PageNumber = 2
	ed, err := svc.QueryEndpoint(ctx, cepr)
	if err != nil {
		t.Fatal(err.Error())
	}
	t.Log(format.FormatToJson(ed))
}
func TestDetailEndpoint(t *testing.T) {
	cepr := endpoint.NewDetailEndpointRequest("3a132f561868bb981279d1d311e4bf781b9714e1")
	ed, err := svc.DetailEndpoint(ctx, cepr)
	if err != nil {
		t.Fatal(err.Error())
	}
	t.Log(format.FormatToJson(ed))
}
