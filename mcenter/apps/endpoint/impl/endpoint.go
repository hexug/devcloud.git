package impl

import (
	"context"

	"gitee.com/hexug/devcloud/mcenter/apps/endpoint"
	"gitee.com/hexug/devcloud/mcenter/apps/service"
	"gitee.com/hexug/devcloud/mcenter/common/logger"
	"gitee.com/hexug/devcloud/mcenter/common/validate"
	"gitee.com/hexug/devcloud/mcenter/test/format"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func (i *implServer) RegisterEndpoint(
	ctx context.Context,
	in *endpoint.RegisterRequest) (
	*endpoint.EndpointSet,
	error) {
	//校验数据
	if err := validate.V().Struct(in); err != nil {
		return nil, err
	}
	es := endpoint.NewEndpointSet()
	for _, crq := range in.Items {
		//检查是否有这个service
		dsr := service.NewDetailServiceRequestByID(crq.ServiceId)
		_, err := i.service.DetailService(ctx, dsr)
		if err != nil {
			logger.L().Errorf("服务 %s 尚未注册\n", crq.ServiceId)
			logger.L().Errorw(err.Error())
			logger.L().Errorw(format.FormatToJson(crq))
			logger.L().Errorw("无法注册")
			continue
		}
		ep, err := endpoint.NewEndpoint(crq)
		if err != nil {
			logger.L().Errorw(err.Error())
			return nil, err
		}
		opt := &options.UpdateOptions{}
		filter := bson.M{"_id": ep.Meta.Id}
		_, err = i.Col.UpdateOne(
			ctx,
			filter,
			bson.M{"$set": ep},
			//启用upsert方法，当重复了，就替换
			opt.SetUpsert(true))
		if err != nil {
			logger.L().Errorw(err.Error())
			return nil, err
		}

		//将下面部分改造成upsert方法
		// depr := endpoint.NewDetailEndpointRequest(ep.Meta.Id)
		// _, err = i.DetailEndpoint(ctx, depr)
		// if err == nil {
		// 	//注册过的端点，直接覆盖
		// 	filter := bson.M{"_id": ep.Meta.Id}
		// 	_, err := i.Col.ReplaceOne(ctx, filter, ep)
		// 	if err != nil {
		// 		logger.L().Errorw(err.Error())
		// 		return nil, err
		// 	}
		// } else {
		// 	//未注册过的端点
		// 	if _, err := i.Col.InsertOne(ctx, ep); err != nil {
		// 		logger.L().Errorw(err.Error())
		// 		return nil, err
		// 	}
		// }
		es.Add(ep)
	}
	return es, nil
}
func (i *implServer) QueryEndpoint(ctx context.Context, in *endpoint.QueryEndpointRequest) (*endpoint.EndpointSet, error) {
	if err := validate.V().Struct(in); err != nil {
		logger.L().Errorw(err.Error())
		return nil, err
	}
	//防止过度获取
	if in.PageSize <= 0 || in.PageSize > 30 {
		in.PageSize = 5
	}
	if in.PageNumber <= 0 || in.PageNumber > 10 {
		in.PageNumber = 1
	}
	es := endpoint.NewEndpointSet()
	filter := bson.M{}
	//按创建时间倒排
	opt := options.Find().
		SetLimit(in.PageSize).
		SetSkip((in.PageNumber - 1) * in.PageSize).
		SetSort(bson.D{{Key: "created_at", Value: -1}}) //按创建时间倒序取出
	cursor, err := i.Col.Find(ctx, filter, opt)
	if err != nil {
		logger.L().Errorw(err.Error())
		return nil, err
	}
	if err := cursor.All(ctx, &es.Items); err != nil {
		logger.L().Errorw(err.Error())
		return nil, err
	}
	count, err := i.Col.CountDocuments(ctx, filter)
	if err != nil {
		logger.L().Errorw(err.Error())
		return nil, err
	}
	es.Total = count
	return es, nil
}
func (i *implServer) DetailEndpoint(ctx context.Context, in *endpoint.DetailEndpointRequest) (*endpoint.Endpoint, error) {
	if err := validate.V().Struct(in); err != nil {
		logger.L().Errorw(err.Error())
		return nil, err
	}
	filter := bson.M{"_id": in.Id}
	res := i.Col.FindOne(ctx, filter)
	if res.Err() != nil {
		logger.L().Errorw(res.Err().Error())
		return nil, res.Err()
	}
	ed, err := endpoint.NewEndpoint(endpoint.NewCreateEndpointRequest())
	if err != nil {
		logger.L().Errorw(err.Error())
		return nil, err
	}
	if err := res.Decode(ed); err != nil {
		logger.L().Errorw(err.Error())
		return nil, err
	}
	return ed, nil
}
