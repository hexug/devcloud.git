package password_test

import (
	"context"
	"testing"

	"gitee.com/hexug/devcloud/mcenter/apps"
	_ "gitee.com/hexug/devcloud/mcenter/apps/all"
	"gitee.com/hexug/devcloud/mcenter/apps/token"
	"gitee.com/hexug/devcloud/mcenter/apps/token/provider"
	"gitee.com/hexug/devcloud/mcenter/conf"
)

var (
	iss provider.Issuer
	ctx = context.TODO()
)

func init() {
	conf.LoadConfig("../../../../etc/config.toml")
	apps.InitApp()
	provider.Init()
	iss = provider.Get(token.GRANT_TYPE_PASSWORD)
}

func TestPassword(t *testing.T) {
	ir := token.NewIssueTokenRequestByPassword("c1", "a")
	tk, err := iss.IssuerToken(ctx, ir)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(tk)
}
