package provider

import (
	"context"

	"gitee.com/hexug/devcloud/mcenter/apps/token"
)

type Issuer interface {
	TokenIssuer
	Init() error
}

type TokenIssuer interface {
	IssuerToken(ctx context.Context, in *token.IssueTokenRequest) (*token.Token, error)
}
