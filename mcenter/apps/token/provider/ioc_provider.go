package provider

import (
	"fmt"

	"gitee.com/hexug/devcloud/mcenter/apps/token"
	"gitee.com/hexug/devcloud/mcenter/common/logger"
)

var (
	providerLib = map[token.GRANT_TYPE]Issuer{}
)

func Registry(key token.GRANT_TYPE, value Issuer) {
	if _, ok := providerLib[key]; ok {
		panic(fmt.Errorf("%d 的Issuer已经注册过了，无发重复注册！", key))
	}
	providerLib[key] = value
}
func Get(key token.GRANT_TYPE) Issuer {
	if v, ok := providerLib[key]; ok {
		return v
	}
	panic(fmt.Errorf("%d 的Issuer尚未注册！", key))
}

func Init() {
	logger.L().Info("注册供应商")
	for _, i := range providerLib {
		if err := i.Init(); err != nil {
			panic(err)
		}
	}
}
