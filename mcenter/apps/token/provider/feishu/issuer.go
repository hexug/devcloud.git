package feishu

import (
	"context"

	"gitee.com/hexug/devcloud/mcenter/apps"
	"gitee.com/hexug/devcloud/mcenter/apps/token"
	"gitee.com/hexug/devcloud/mcenter/apps/token/provider"
	"gitee.com/hexug/devcloud/mcenter/apps/user"
)

var _ provider.Issuer = (*issuer)(nil)

type issuer struct {
	svc user.Server
}

func (i *issuer) Init() error {
	i.svc = apps.GetApp(user.AppName).(user.Server)
	return nil
}
func (i *issuer) IssuerToken(ctx context.Context, in *token.IssueTokenRequest) (*token.Token, error) {
	//TODO 未实现
	return nil, nil
}
func init() {
	provider.Registry(token.GRANT_TYPE_FEISHU, &issuer{})
}
