package token

import context "context"

var (
	AppName = "tokens"
)

type Server interface {
	RPCServer
	//颁发token
	IssueToken(context.Context, *IssueTokenRequest) (*Token, error)
	//删除token
	DeleteToken(context.Context, *DeleteTokenRequest) (*Token, error)
}

func NewIssueTokenRequestByPassword(username, password string) *IssueTokenRequest {
	return &IssueTokenRequest{
		GrantType: GRANT_TYPE_PASSWORD,
		Username:  username,
		Password:  password,
	}
}
func NewIssueTokenRequestByPasswordApi() *IssueTokenRequest {
	return &IssueTokenRequest{
		GrantType: GRANT_TYPE_PASSWORD,
	}
}
func NewValidateTokenRequest(username string) *ValidateTokenRequest {
	return &ValidateTokenRequest{
		Username: username,
	}
}
func NewDeleteTokenRequest() *DeleteTokenRequest {
	return &DeleteTokenRequest{
		GrantType: GRANT_TYPE_PASSWORD,
	}
}
func NewDeleteTokenRequestByPassword(username, tk string) *DeleteTokenRequest {
	return &DeleteTokenRequest{
		GrantType:   GRANT_TYPE_PASSWORD,
		Username:    username,
		AccessToken: tk,
	}
}
