package impl_test

import (
	"context"
	"testing"

	"gitee.com/hexug/devcloud/mcenter/apps"
	_ "gitee.com/hexug/devcloud/mcenter/apps/all"
	"gitee.com/hexug/devcloud/mcenter/apps/token"
	"gitee.com/hexug/devcloud/mcenter/apps/token/provider"
	"gitee.com/hexug/devcloud/mcenter/conf"
	"gitee.com/hexug/devcloud/mcenter/test/format"
)

var (
	svc token.Server
	ctx = context.TODO()
)

func init() {
	conf.LoadConfig("../../../etc/config.toml")
	apps.InitApp()
	provider.Init()
	svc = apps.GetApp(token.AppName).(token.Server)
}

func TestIssueToken(t *testing.T) {
	ir := token.NewIssueTokenRequestByPassword("c", "a")
	tk, err := svc.IssueToken(ctx, ir)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(format.FormatToJson(tk))
}
func TestValidateToken(t *testing.T) {
	ir := token.NewValidateTokenRequest("c")
	ir.AccessToken = "41ba7382-db62-4704-97e7-5c3f48d087a1"
	tk, err := svc.ValidateToken(ctx, ir)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(tk)
}
func TestDeleteToken(t *testing.T) {
	ir := token.NewDeleteTokenRequestByPassword("c", "b262a3fb-fc89-428a-be5b-11aa28854314")
	tk, err := svc.DeleteToken(ctx, ir)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(tk)
}
