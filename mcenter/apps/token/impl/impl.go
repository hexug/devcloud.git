package impl

import (
	"fmt"

	"gitee.com/hexug/devcloud/mcenter/apps"
	"gitee.com/hexug/devcloud/mcenter/apps/token"
	"gitee.com/hexug/devcloud/mcenter/apps/token/provider"
	_ "gitee.com/hexug/devcloud/mcenter/apps/token/provider/all"
	"gitee.com/hexug/devcloud/mcenter/conf"
	"go.mongodb.org/mongo-driver/mongo"
	"google.golang.org/grpc"
)

// 约束
var _ token.Server = (*implServer)(nil)
var _ token.RPCServer = (*implServer)(nil)
var _ apps.AppObj = (*implServer)(nil)
var _ apps.GrpcObj = (*implServer)(nil)

type implServer struct {
	token.UnimplementedRPCServer
	Col *mongo.Collection
}

var (
	svc = &implServer{}
)

func (i *implServer) Name() string {
	return token.AppName
}
func (i *implServer) Init() error {
	//供应商初始化
	provider.Init()
	i.Col = conf.DB().MongoClient.Collection(token.AppName)
	if i.Col == nil {
		return fmt.Errorf("数据库 %s 获取失败,疑似尚未注册", token.AppName)
	}
	return nil
}
func (i *implServer) ReagisterGrpc(s *grpc.Server) {
	token.RegisterRPCServer(s, i)
}

func init() {
	apps.RegisterApp(svc)
	apps.RegisterGrpc(svc)
}
