package main

import (
	_ "gitee.com/hexug/devcloud/mcenter/apps/all"
	"gitee.com/hexug/devcloud/mcenter/cmd"
)

func main() {
	cmd.Execute()
}
