package apps

import (
	"errors"
	"fmt"
)

var (
	AppObjLib = map[string]AppObj{}
)

type AppObj interface {
	Name() string
	Init() error
}

func RegisterApp(obj AppObj) {
	if _, ok := AppObjLib[obj.Name()]; ok {
		panic(errors.New("app " + obj.Name() + " 已注册过！"))
	} else {
		AppObjLib[obj.Name()] = obj
		fmt.Printf("app %s 注册成功。\n", obj.Name())
	}
}

func GetApp(name string) AppObj {
	fmt.Println(name, AppObjLib)
	if value, ok := AppObjLib[name]; ok {
		return value
	} else {
		panic(errors.New("app " + name + "尚未注册！"))
	}
}
