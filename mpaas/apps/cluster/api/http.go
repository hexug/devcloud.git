package api

import (
	"gitee.com/hexug/devcloud/mpaas/apps"
	"gitee.com/hexug/devcloud/mpaas/apps/cluster"
	restfulspec "github.com/emicklei/go-restful-openapi/v2"
	"github.com/emicklei/go-restful/v3"
)

var _ apps.HttpObj = (*ApiServer)(nil)

type ApiServer struct {
	Svc cluster.Server
}

func (a *ApiServer) Name() string {
	return cluster.AppName
}

func (a *ApiServer) Init() error {
	// a.Svc = apps.GetApp(cluster.AppName).(cluster.Server)
	// if a.Svc == nil {
	// 	return fmt.Errorf("app %s 获取失败,疑似尚未注册", cluster.AppName)
	// }
	return nil
}
func (h *ApiServer) Version() string {
	return "v1"
}

// 注册路由
func (a *ApiServer) ReagisterRoute(ws *restful.WebService) {
	tags := []string{"集群"}
	//设置子路由
	// http://127.0.0.1:2345/mcenter/api/v1/users  post
	ws.Route(ws.GET("/").To(a.QueryCluster).
		//设置路由的描述信息
		Doc("查询集群").
		//设置路由的所属组标签
		Metadata(restfulspec.KeyOpenAPITags, tags).Metadata("auth", true).Metadata("perm", true))
}

func init() {
	apps.RegisterHttp(&ApiServer{})
}
