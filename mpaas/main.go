package main

import (
	_ "gitee.com/hexug/devcloud/mpaas/apps/all"
	"gitee.com/hexug/devcloud/mpaas/cmd"
)

func main() {
	cmd.Execute()
}
