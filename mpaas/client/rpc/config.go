package rpc

import "time"

type Config struct {
	TimeoutSecond uint64 `json:"timeout_second"`
	Address       string `json:"address"`
}

func NewConfig() *Config {
	return &Config{
		Address:       "localhost:12345",
		TimeoutSecond: 10,
	}
}

func (c *Config) Timeout() time.Duration {
	return time.Duration(c.TimeoutSecond) * time.Second
}
