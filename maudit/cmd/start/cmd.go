package start

import (
	"context"
	"fmt"
	"os"
	"os/signal"
	"strings"
	"syscall"

	"gitee.com/hexug/devcloud/maudit/apps"
	"gitee.com/hexug/devcloud/maudit/common/logger"
	"gitee.com/hexug/devcloud/maudit/conf"
	"gitee.com/hexug/devcloud/maudit/controller"
	"gitee.com/hexug/devcloud/maudit/protocol"
	"github.com/spf13/cobra"
)

var (
	Path string
	Open bool
	Cmd  = &cobra.Command{
		Use:   "start",
		Short: "启动服务",
		Long:  "启动maudit服务",

		Run: func(cmd *cobra.Command, args []string) {
			conf.LoadConfig(Path)
			fmt.Println("配置文件读取完成")

			//接收信号
			ch := make(chan os.Signal, 1)
			defer close(ch)
			signal.Notify(ch, syscall.SIGTERM, syscall.SIGINT, syscall.SIGHUP, syscall.SIGQUIT)

			//初始化app
			apps.InitApp()

			// 初始化服务
			svr, err := newService()
			if err != nil {
				panic(err)
			}

			//监听信号
			go svr.waitSign(ch)
			// if Open {
			// 	fmt.Println("打开浏览器")
			// 	go func() {
			// 		exec.Command("cmd", "/C", "start "+"http://127.0.0.1:2346/apidocs.json").Run()
			// 	}()
			// }
			//启服务
			if err := svr.start(); err != nil {
				if !strings.Contains(err.Error(), "http: Server closed") {
					panic(err)
				}
			}
		},
	}
)

func init() {
	Cmd.Flags().StringVarP(&Path, "path", "f", "etc/config.toml", "配置文件信息")
	Cmd.Flags().BoolVarP(&Open, "web", "w", false, "打开浏览器")
}

type service struct {
	http   *protocol.HTTPService
	grpc   *protocol.GRPCService
	auditc *controller.AuduitLogSaveConroller
}

func newService() (*service, error) {
	grpc := protocol.NewGRPCService()
	http := protocol.NewHTTPService()
	svr := &service{
		http: http,
		grpc: grpc,
	}

	return svr, nil
}

// 当接收到信号
func (s *service) waitSign(sign chan os.Signal) {
	//遍历管道
	for sg := range sign {
		//查看类型
		switch v := sg.(type) {
		//符合上面定义的管道的信号就会进来
		default:
			logger.L().Infof("接收到的信号： %v, 开始优雅的关闭服务", v.String())

			//关闭controller
			if err := s.auditc.Close(); err != nil {
				logger.L().Infof("controller服务优雅的关闭失败: %s, 强制退出", err)
			} else {
				logger.L().Info("controller服务关闭完成")
			}
			//先关闭grpc
			if err := s.grpc.Stop(); err != nil {
				logger.L().Infof("grpc服务优雅的关闭失败: %s, 强制退出", err)
			} else {
				logger.L().Info("grpc服务关闭完成")
			}
			//再关闭http服务
			if err := s.http.Stop(); err != nil {
				logger.L().Infof("http服务优雅的关闭失败: %s, force exit", err)
			} else {
				logger.L().Info("http服务关闭完成")
			}
			return
		}
	}
}

func (s *service) start() error {
	logger.L().Infof("注册过的grpc: %s", apps.InitedGrpc())
	logger.L().Infof("注册过的http api: %s", apps.InitedHttp())

	logger.L().Infof("注册过的app: %s", apps.InitedApp())
	s.auditc = controller.NewAuduitLogSaveConroller(
		conf.C().GetStringSlice("kafka.broker"),
		conf.C().GetString("kafka.group_id"),
		conf.C().GetString("kafka.topic"),
	)
	go s.auditc.Run(context.Background())
	logger.L().Infof("启动controller, 消费组为: %s", conf.C().GetString("kafka.group_id"))

	go s.grpc.Start()
	return s.http.Start()
}
