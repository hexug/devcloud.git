package cmd

import (
	"fmt"

	"gitee.com/hexug/devcloud/maudit/cmd/initial"
	"gitee.com/hexug/devcloud/maudit/cmd/start"
	"gitee.com/hexug/devcloud/maudit/version"
	"github.com/spf13/cobra"
)

var (
	v bool
	V bool
	//创建根命令
	rootCmd = &cobra.Command{
		Use: "mpaas",
		// short是“help”输出中显示的简短描述。
		Short: "mpaas 管理中心",
		// Long是“help＜this command＞”输出中显示的长消息。
		Long: "mpaas 管理中心",
		//Example是如何使用该命令的示例。
		Example: "mpaas  -v",
		Run: func(cmd *cobra.Command, args []string) {
			//当带了version参数，就打印版本号
			if v {
				fmt.Println(version.ShortVersion())
				return
			}
			if V {
				fmt.Println(version.FullVersion())
				return
			}
			//否则打印帮助信息
			err := cmd.Help()
			cobra.CheckErr(err)
		},
	}
)

func Execute() {
	//添加其他子命令
	rootCmd.AddCommand(initial.Cmd)
	rootCmd.AddCommand(start.Cmd)

	//执行根命令的入口函数
	if err := rootCmd.Execute(); err != nil {
		//fmt.Fprintln(os.Stderr, err)
		//os.Exit(1)
		return
	}
}

// 初始化的时候获取参数的值 绑定了一个本地变量version 用来显示版本
func init() {
	rootCmd.Flags().BoolVarP(&v, "version", "v", false, "版本信息")
	rootCmd.Flags().BoolVarP(&V, "Version", "V", false, "长版本信息")
}
