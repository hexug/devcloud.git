package validate_test

import (
	"testing"

	"gitee.com/hexug/devcloud/maudit/common/validate"
)

type a struct {
	C string `validate:"required"`
	B string `validate:"required"`
}

func TestValidate(t *testing.T) {
	A := a{}
	if err := validate.V().Struct(A); err != nil {
		t.Fatal(err)
	}
	t.Log("成功了")
}
