package validate

import "github.com/go-playground/validator/v10"

var validate *validator.Validate

/*
用法

		//新建结构体加validate标签
		type RegistRequest struct {
	        //调用校验方法
			UserName string `validate:"required"` //自定义校验
			Passwd   string `validate:"required"`
		}
		//新建对象
		reg := RegistRequest{
			UserName: "abc",  //通过
			Passwd:   "ab1c", //不通过
		}
		//校验
		if err :=V().Struct(reg);err!=nil{
			painc(err)
		}
*/
func V() *validator.Validate {
	return validate
}

func init() {
	validate = validator.New()
	//注册其他自定义的校验标签
	//validate.RegisterValidation("validation_tag", func(fl validator.FieldLevel) bool {
	//	数据校验逻辑
	//})
}
