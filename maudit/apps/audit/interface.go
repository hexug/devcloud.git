package audit

import "context"

var (
	AppName = "audits"
)

type Server interface {
	// 日志保存
	SaveAuditLog(ctx context.Context, in *AuditLog) (*AuditLog, error)
	// 日志查询
	QueryAuditLog(ctx context.Context, in *QueryAuditLogRequest) (*AuditLogSet, error)
}
