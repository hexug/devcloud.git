package impl

import (
	"context"
	"fmt"

	"gitee.com/hexug/devcloud/maudit/apps/audit"
	"gitee.com/hexug/devcloud/maudit/common/logger"
	"gitee.com/hexug/devcloud/maudit/test/format"
)

func (i *implServer) SaveAuditLog(ctx context.Context, in *audit.AuditLog) (*audit.AuditLog, error) {
	fmt.Println(format.FormatToJson(in))
	_, err := i.Col.InsertOne(ctx, in)
	if err != nil {
		logger.L().Errorf("保存数据库出错: %s", err.Error())
		return nil, err
	}
	return in, nil
}

// 日志查询
func (i *implServer) QueryAuditLog(ctx context.Context, in *audit.QueryAuditLogRequest) (*audit.AuditLogSet, error) {
	//TODO 尚未实现具体功能
	return nil, nil
}
