package impl

import (
	"fmt"

	"gitee.com/hexug/devcloud/maudit/apps"
	"gitee.com/hexug/devcloud/maudit/apps/audit"
	"gitee.com/hexug/devcloud/maudit/conf"
	"go.mongodb.org/mongo-driver/mongo"
)

// 约束
var _ audit.Server = (*implServer)(nil)
var _ apps.AppObj = (*implServer)(nil)

type implServer struct {
	Col *mongo.Collection
}

var (
	svc = &implServer{}
)

func (i *implServer) Name() string {
	return audit.AppName
}
func (i *implServer) Init() error {
	i.Col = conf.DB().MongoClient.Collection(audit.AppName)
	if i.Col == nil {
		return fmt.Errorf("数据库 %s 获取失败,疑似尚未注册", audit.AppName)
	}
	return nil
}

func init() {
	apps.RegisterApp(svc)
}
