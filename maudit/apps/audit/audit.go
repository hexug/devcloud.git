package audit

import (
	"encoding/json"
	"time"

	"gitee.com/hexug/devcloud/maudit/common/logger"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type AuditLog struct {
	Id        primitive.ObjectID `bson:"_id" json:"id"`
	Username  string             `bson:"username" json:"username"`     // 操作的用户
	Time      int64              `bson:"time" json:"time"`             //操作的时间
	ServiceId string             `bson:"service_id" json:"service_id"` //操作的服务
	Operate   string             `bson:"operate" json:"operate"`       //具体操作
	Request   string             `bson:"request" json:"request"`       //具体请求内容
}

func NewAuditLog() *AuditLog {
	return &AuditLog{}
}

func NewCreateAuditLog() *AuditLog {
	return &AuditLog{
		Id:   primitive.NewObjectID(),
		Time: time.Now().Unix(),
	}
}

func LoadAuditLogJsonFromByte(s []byte) *AuditLog {
	ad := NewAuditLog()
	err := json.Unmarshal(s, ad)
	if err != nil {
		logger.L().Errorf("审计日志LoadAuditLogJsonFromByte出错: %s", err.Error())
		return nil
	}
	return ad
}

func (a *AuditLog) ToJsonByte() []byte {
	b, err := json.Marshal(a)
	if err != nil {
		logger.L().Errorf("审计日志转化Byte出错: %s", err.Error())
		logger.L().Errorf("错误的日志是： %+v", a)
		return nil
	}
	return b
}
func (a *AuditLog) String() string {
	b := a.ToJsonByte()
	if b != nil {
		return string(b)
	}
	logger.L().Error("审计日志转化字符串出错")
	logger.L().Errorf("错误的日志是： %+v", a)
	return ""
}

type QueryAuditLogRequest struct {
	PageSize   int64  `json:"page_size"`
	PageNumber int64  `json:"page_number"`
	Keyword    string `json:"keyword"`
}

type AuditLogSet struct {
	// 总日志数
	Total int64       `json:"total" bson:"total"`
	Items []*AuditLog `json:"items" bson:"items"`
}
