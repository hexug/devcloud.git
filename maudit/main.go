package main

import (
	_ "gitee.com/hexug/devcloud/maudit/apps/all"
	"gitee.com/hexug/devcloud/maudit/cmd"
)

func main() {
	cmd.Execute()
}
