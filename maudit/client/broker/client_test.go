package broker_test

import (
	"context"
	"log"
	"testing"

	"gitee.com/hexug/devcloud/maudit/apps/audit"
	"gitee.com/hexug/devcloud/maudit/client/broker"
	"gitee.com/hexug/devcloud/maudit/conf"
	"gitee.com/hexug/devcloud/maudit/test/format"
	"github.com/segmentio/kafka-go"
)

var (
	ctx = context.Background()
)

func init() {
	conf.LoadConfig("../../etc/config.toml")
}

func TestProducer(t *testing.T) {
	w := &kafka.Writer{
		Addr:                   kafka.TCP("10.0.0.30:9093"),
		Topic:                  "topic-A",
		Balancer:               &kafka.LeastBytes{},
		AllowAutoTopicCreation: true,
	}

	err := w.WriteMessages(context.Background(),
		kafka.Message{
			Value: []byte("Hello World!"),
		},
	)

	if err != nil {
		log.Fatal("failed to write messages:", err)
	}

	if err := w.Close(); err != nil {
		log.Fatal("failed to close writer:", err)
	}
}

func TestSendAuditLog(t *testing.T) {
	client := broker.NewDefaultClient(conf.C().GetStringSlice("kafka.broker"))
	ad := audit.NewCreateAuditLog()
	ad.Username = "abcd"
	ad.ServiceId = "serviceID 2"
	ad.Request = "aaa/a/a/a"
	ad.Operate = "XXXXXX"
	err := client.SendAuditLog(ctx, ad)
	if err != nil {
		t.Fatal(err.Error())
	}
	t.Log(format.FormatToJson(ad))
}
