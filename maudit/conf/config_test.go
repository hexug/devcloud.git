package conf_test

import (
	"context"
	"fmt"
	"testing"

	"gitee.com/hexug/devcloud/maudit/conf"
	"go.mongodb.org/mongo-driver/bson"
)

var (
	ctx = context.TODO()
)

func init() {
	conf.LoadConfig("../etc/config.toml")
}
func TestLoadConfig(t *testing.T) {
	fmt.Println(conf.C().Get("auth.username"))
	db := conf.DB().MongoClient
	res, err := db.Collection("inventory").DeleteMany(ctx, bson.M{"size.h": 10})
	if err != nil {
		t.Fatal(err)
	}
	t.Log(res)

}
