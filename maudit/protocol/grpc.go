package protocol

import (
	"fmt"
	"net"

	"gitee.com/hexug/devcloud/maudit/apps"
	"gitee.com/hexug/devcloud/maudit/common/logger"
	"gitee.com/hexug/devcloud/maudit/conf"
	"google.golang.org/grpc"
)

// NewGRPCService todo
func NewGRPCService() *GRPCService {
	grpcServer := grpc.NewServer()

	return &GRPCService{
		svr: grpcServer,
	}
}

// GRPCService grpc服务
type GRPCService struct {
	svr *grpc.Server
}

// Start 启动GRPC服务
func (s *GRPCService) Start() {
	// 装载所有GRPC服务
	// app.LoadGrpcApp(s.svr)
	apps.InitGrpc(s.svr)
	// 启动HTTP服务
	lis, err := net.Listen("tcp", fmt.Sprintf("%s:%d", conf.C().Get("grpc.host"), conf.C().Get("grpc.port")))
	if err != nil {
		logger.L().Debugf("listen grpc tcp conn error, %s", err)
		return
	}

	logger.L().Infof("GRPC 服务监听地址: %s:%d", conf.C().Get("grpc.host"), conf.C().Get("grpc.port"))
	if err := s.svr.Serve(lis); err != nil {
		if err == grpc.ErrServerStopped {
			logger.L().Info("服务停止")
		}

		logger.L().Errorf("启动grpc出错, %s", err.Error())
		return
	}
}

// Stop 关闭GRPC服务
func (s *GRPCService) Stop() error {
	s.svr.GracefulStop()
	return nil
}
